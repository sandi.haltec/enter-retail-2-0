            <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
            </footer>
        </div>
    </div>

    <script src="{{ asset('js/config.js') }}"></script>
    <script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.15/filtering/row-based/range_dates.js"></script>
    <script src="{{ asset('js/confirm.js') }}"></script>
    @stack('scripts')
    <script src="{{ asset('build/js/custom.min.js') }}"></script>
</body>
</html>