@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Tambah Beli Retur</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            {!! Form::open(['route' => 'beli_retur.store', 'class' => 'form-horizontal']) !!}
                                @include('beli_retur.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ url('beli_retur') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection