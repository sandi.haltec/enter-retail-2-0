@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('beli_retur/create') }}" class="btn btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Beli Retur</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>ID Beli</td>
                                        <td>ID Barang</td>
                                        <td>Tanggal</td>
                                        <td>Kuantitas</td>
                                        <td>Diskon</td>
                                        <td>Harga</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/beli_retur/get',
                columns:[
                    {data: 'id_beli', name: 'id_beli'},
                    {data: 'id_barang', name: 'id_barang'},
                    {data: 'tanggal', name: 'tanggal'},
                    {data: 'kuantitas', name: 'kuantitas'},
                    {data: 'diskon', name: 'diskon'},
                    {data: 'harga', name: 'harga'},
                    {data: 'id_beli_retur', name: 'id_beli_retur', render: function(data, type, row) {
                        html = '<a onclick="confirm(\'beli_retur/destroy/'+data+'\')" class="btn btn-sm btn-danger pull-right"><i class="fa fa-trash"></i></a>';
                        return html += '<a href="beli_retur/edit/'+data+'" class="btn btn-sm btn-warning pull-right"><span class="fa fa-edit"></span></a>';
                    }},
                ]
            });
        });
    </script>
@endpush