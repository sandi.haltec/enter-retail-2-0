<div class="form-group{{ $errors->has('id_beli') ? ' has-error' : '' }}">
  {!! Form::label('id_beli', 'Pembelian', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_beli', [''=>'Pilih Nomor Faktur', '1'=>'123', '2'=>'111'], null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_beli', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('id_barang') ? ' has-error' : '' }}">
  {!! Form::label('id_barang', 'Barang', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_barang', [''=>'Pilih Barang']+App\Model\Barang::pluck('nama_barang','id_barang')->all(), null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_barang', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
  {!! Form::label('tanggal', 'Tanggal', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::text('tanggal', null, ['class'=>'form-control datepicker']) !!}
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
      </span>
    </div>
    {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('kuantitas') ? ' has-error' : '' }}">
  {!! Form::label('kuantitas', 'Kuantitas', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('kuantitas', null, ['class'=>'form-control']) !!}
    {!! $errors->first('kuantitas', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('diskon') ? ' has-error' : '' }}">
  {!! Form::label('diskon', 'Diskon', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('diskon', null, ['class'=>'form-control']) !!}
    {!! $errors->first('diskon', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
  {!! Form::label('harga', 'Harga', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('harga', null, ['class'=>'form-control text-right']) !!}
    {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="ln_solid"></div> 
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();
        $('.datepicker').datepicker({
            format:'yyyy-mm-dd'
        });
    </script>
@endpush