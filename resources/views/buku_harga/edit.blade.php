@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Tambah Buku harga</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            {!! Form::model($buku_harga, ['route' => ['buku_harga.update', $buku_harga->id_buku_harga], 'class' => 'form-horizontal']) !!}
                                @include('buku_harga.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ url('buku_harga') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a>
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection