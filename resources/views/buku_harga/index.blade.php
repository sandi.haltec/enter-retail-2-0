@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('buku_harga/create') }}" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span></a>
                    <a href="{{ url('buku_harga/export') }}" class="btn btn-sm btn-success"><i class="fa fa-download"></i></a>
                    <button onclick="showImportModal()" class="btn btn-sm btn-success"><i class="fa fa-upload"></i></button>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Buku Harga</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Nama Barang</td>
                                        <td>Nama Pelanggan</td>
                                        <td>Harga</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="importModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            {!! Form::open(['route' => 'buku_harga.import', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Import Data Buku Harga</h4>
                </div>
                <div class="modal-body">
                    <input type="file" name="excel_file" class="btn btn-default" style="width: 100%; height: 200px;">
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <a href="{{ route('buku_harga.example') }}" class="btn btn-default"><i class="fa fa-download"></i></a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i></button>
                        <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function(){
            $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/buku_harga/get',
                columns: [
                    {data: 'id_barang', name: 'id_barang'},
                    {data: 'id_pelanggan', name: 'id_pelanggan'},
                    {data: 'harga', name: 'harga'},
                    {data: 'id_buku_harga', name: 'id_buku_harga', render: function(data, type, row) {
                        html = '<a onclick="confirm(\'buku_harga/destroy/'+data+'\')" class="btn btn-sm btn-danger pull-right"><i class="fa fa-trash"></i></a>';
                        return html += '<a href="buku_harga/edit/'+data+'" class="btn btn-sm btn-warning pull-right"><span class="fa fa-edit"></span></a>';
                    }},
                ]
            });
        });
        function showImportModal() {
            $('#importModal').modal('show');
        }
    </script>
@endpush