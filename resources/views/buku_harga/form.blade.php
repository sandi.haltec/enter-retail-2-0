<div class="form-group{{ $errors->has('id_barang') ? ' has-error' : '' }}">
  {!! Form::label('id_barang', 'Barang', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_barang', [''=>'Pilih Barang']+App\Model\Barang::pluck('nama_barang','id_barang')->all(), null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_barang', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('id_pelanggan') ? ' has-error' : '' }}">
  {!! Form::label('id_pelanggan', 'Pelanggan', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_pelanggan', [''=>'Pilih Pelanggan']+App\Model\Pelanggan::pluck('nama_pelanggan', 'id_pelanggan')->all(), null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_pelanggan', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
  {!! Form::label('harga', 'Harga', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('harga', null, ['class'=>'form-control']) !!}
    {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="ln_solid"></div> 
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();
    </script>
@endpush