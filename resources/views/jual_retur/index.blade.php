@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Jual Retur</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>ID Jual</td>
                                        <td>ID Barang</td>
                                        <td>Tanggal</td>
                                        <td>Kuantitas</td>
                                        <td>Diskon</td>
                                        <td>Harga</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/jual_retur/get',
                columns:[
                    {data: 'id_jual', name: 'id_jual'},
                    {data: 'id_barang', name: 'id_barang'},
                    {data: 'tanggal', name: 'tanggal'},
                    {data: 'kuantitas', name: 'kuantitas'},
                    {data: 'diskon', name: 'diskon'},
                    {data: 'harga', name: 'harga'},
                    {data: 'id_jual_retur', name: 'id_jual_retur', render: function(data, type, row) {
                        html = '<a onclick="confirm(\'jual_retur/destroy/'+data+'\')" class="btn btn-sm btn-danger pull-right"><i class="fa fa-trash"></i></a>';
                        return html += '<a href="jual_retur/edit/'+data+'" class="btn btn-sm btn-warning pull-right"><span class="fa fa-edit"></span></a>';
                    }},
                ]
            });
        });
    </script>
@endpush