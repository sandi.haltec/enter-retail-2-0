<div class="form-group{{ $errors->has('id_jual') ? ' has-error' : '' }}">
  {!! Form::label('id_jual', 'Penjualan', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_jual', [''=>'Pilih Nomor Faktur', '1'=>'123', '2'=>'111'], null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_jual', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('id_barang') ? ' has-error' : '' }}">
  {!! Form::label('id_barang', 'Barang', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('id_barang', [''=>'Pilih Barang']+App\Model\Barang::pluck('nama_barang','id_barang')->all(), null, ['class'=>'form-control select2']) !!}
    {!! $errors->first('id_barang', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
  {!! Form::label('tanggal', 'Tanggal', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::text('tanggal', null, ['class'=>'form-control datepicker']) !!}
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
      </span>
    </div>
    {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('kuantitas') ? ' has-error' : '' }}">
  {!! Form::label('kuantitas', 'Kuantitas', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('kuantitas', null, ['class'=>'form-control', 'onkeyup'=>'countTotal()']) !!}
    {!! $errors->first('kuantitas', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('diskon') ? ' has-error' : '' }}">
  {!! Form::label('diskon', 'Diskon', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('diskon', null, ['class'=>'form-control text-right input-number', 'onkeyup'=>'countTotal()']) !!}
    {!! $errors->first('diskon', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
  {!! Form::label('harga', 'Harga', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('harga', null, ['class'=>'form-control text-right input-number', 'onkeyup'=>'countTotal()']) !!}
    {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('total') ? ' has-error' : '' }}">
  {!! Form::label('total', 'Total', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('total', null, ['class'=>'form-control text-right input-number', 'readonly'=>'readonly']) !!}
    {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('alasan') ? ' has-error' : '' }}">
  {!! Form::label('alasan', 'Alasan', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::textarea('alasan', null, ['rows'=>'4', 'class'=>'form-control']) !!}
    {!! $errors->first('alasan', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="ln_solid"></div> 
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="http://localhost:8000/vendors/jquery-number/jquery.number.min.js"></script>

    <script>
        $('.select2').select2();
        $('.datepicker').datepicker({
            format:'yyyy-mm-dd'
        });
        $('.input-number').number(true, 2);

        function countTotal() {
          var kuantitas = $('input[name="kuantitas"]').val();
          var diskon = $('input[name="diskon"]').val();
          var harga = $('input[name="harga"]').val();
            if(!$.isNumeric(kuantitas)){
                kuantitas=0;
            }
            if(!$.isNumeric(diskon)){
                diskon=0;
            }
            if(!$.isNumeric(harga)){
                harga=0;
            }
          var total = (kuantitas*harga)-diskon;

          $('input[name="total"]').val(total);
        }
        // alert(kuantitas);
    </script>
@endpush