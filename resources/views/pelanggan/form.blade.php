<div class="form-group"> 
    {!! Form::label('nama_pelanggan', 'Nama *', ['class' => 'control-label col-md-2']) !!} 
    <div class="col-md-6"> 
        {!! Form::text('nama_pelanggan', null, ['class' => 'form-control col-md-8']) !!} 
    </div> 
</div> 
<div class="form-group"> 
    {!! Form::label('no_telp', 'No Telp *', ['class' => 'control-label col-md-2']) !!} 
    <div class="col-md-6"> 
        {!! Form::text('no_telp', null, ['class' => 'form-control col-md-8']) !!} 
    </div> 
</div>
<div class="form-group"> 
    {!! Form::label('email', 'Email *', ['class' => 'control-label col-md-2']) !!} 
    <div class="col-md-6"> 
        {!! Form::text('email', null, ['class' => 'form-control col-md-8']) !!} 
    </div> 
</div>
<div class="form-group"> 
    {!! Form::label('alamat', 'Alamat *', ['class' => 'control-label col-md-2']) !!} 
    <div class="col-md-6"> 
        {!! Form::textarea('alamat', null, ['rows' => '3', 'class' => 'form-control col-md-8']) !!} 
    </div> 
</div> 
<div class="ln_solid"></div>