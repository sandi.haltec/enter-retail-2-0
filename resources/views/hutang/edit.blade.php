@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Hutang</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                        	{!! Form::model($hutang, ['route' => ['hutang.update', $hutang->id_pembayaran], 'class' => 'form-horizontal'])  !!}
                                @include('hutang.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ url('hutang') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
        });
        function getFaktur() {
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = 'http://localhost:8000/hutang/getFaktur/'+faktur;
            $.ajax({
                url:base_url,
                type:'get',
                dataType:'json',
                data:'faktur='+faktur,
                success:function(response) {
                    if (response['err']) {
                        alert('Data tidak ditemukan.');
                    }else{
                        alert('Data sudah ditemukan.');
                        $('input[name="id_beli"').val(response);
                    }
                }
            });
        };
    </script>
@endpush