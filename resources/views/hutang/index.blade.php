@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('hutang/create') }}" data-toggle="tooltip" title="Tambah Data" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Hutang</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>No Faktur</td>
                                        <td>Jatuh Tempo</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            var table = $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/hutang/get',
                columns:[
                    {data: 'no_faktur', name: 'no_faktur'},
                    {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    {data: 'id_beli', name: 'id_beli', orderable: false, render: function(data, type, row) {
                        html = '<a onclick="confirm(\'hutang/destroy/'+data+'\')" data-toggle="tooltip" title="Hapus Data" class="btn btn-sm btn-danger pull-right"><i class="fa fa-trash"></i></a>';
                        return html += '<a href="hutang/edit/'+data+'" data-toggle="tooltip" title="Edit Data" class="btn btn-sm btn-warning pull-right"><span class="fa fa-edit"></span></a>';
                    }},
                ]
            });

            table.column(2).data('asd');
        });
    </script>
@endpush