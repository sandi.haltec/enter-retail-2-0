@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Detail</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            <div class="panel-body">                            
                                <ul class="nav nav-tabs">                   
                                    <li class="active"><a href="#tab-customer" data-toggle="tab">Supplier</a></li>
                                    <li><a href="#tab-items" data-toggle="tab">Barang</a></li>
                                    <li><a href="#tab-others" data-toggle="tab">Biaya Lain-lain</a></li>
                                    <li><a href="#tab-payments" data-toggle="tab">Pembayaran</a></li>
                                    <li><a href="#tab-detail" data-toggle="tab">Detail</a></li>                 
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-customer">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th class="col-sm-2">Nama</th>
                                                <td>{{ $detail->supplier->nama_supplier }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ $detail->supplier->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <th>No. Telepon</th>
                                                <td>{{ $detail->supplier->no_telp }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{ $detail->supplier->email }}</td>
                                            </tr>                       
                                        </table>                    
                                    </div>
                                    <div class="tab-pane" id="tab-items">                       
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-left">Kode Barang</th>
                                                    <th class="text-left">Barcode</th>
                                                    <th class="text-left">Nama</th>
                                                    <th class="text-center">Jumlah</th>
                                                    <th class="text-right">Harga</th>
                                                    <th class="text-center">Diskon</th>
                                                    <th class="text-right">Total</th>                               
                                                </tr>                           
                                            </thead>
                                            <tbody>
                                                <?php $grandTotal = 0 ?>
                                                @foreach($detail->beliBarang()->get() as $beliBarang)
                                                    <tr>
                                                        <td class="text-left">{{ $beliBarang->Barang()->first()->kode_barang }}</td>
                                                        <td class="text-left">{{ $beliBarang->Barang()->first()->barcode }}</td>
                                                        <td class="text-left">{{ $beliBarang->Barang()->first()->nama_barang }}</td>
                                                        <td class="text-center">{{ $beliBarang->jumlah }}</td>
                                                        <td class="text-right">{{ $beliBarang->harga }}</td>
                                                        <td class="text-center">{{ $beliBarang->diskon }}</td>
                                                        <td class="text-right">{{ $beliBarang->total }}</td>
                                                    </tr>
                                                    <?php 
                                                        $grandTotal += $beliBarang->total;
                                                     ?>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-left" colspan="6">Grand Total</th>                              
                                                    <td class="text-right">{{ $grandTotal }}</td>                              
                                                </tr>
                                            </tfoot>                                                
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="tab-others">
                                        <table id="others" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-left">Keterangan</th>                                   
                                                    <th class="text-right">Harga</th>                             
                                                </tr>                           
                                            </thead>
                                            <tbody>
                                                <?php $biayaLain = 0 ?>
                                                @foreach($detail->beliLain()->get() as $beliLain)
                                                <tr>
                                                    <td class="text-left">{{ $beliLain->deskripsi }}</td>
                                                    <td class="text-right">{{ $beliLain->harga }}</td>
                                                    <?php 
                                                        $biayaLain += $beliLain->harga;
                                                     ?>                               
                                                </tr>
                                                @endforeach
                                            </tbody>    
                                            <tfoot>
                                                <tr>
                                                    <th class="text-left">Biaya Lain-lain</th>                              
                                                    <td class="text-right">{{ $biayaLain }}</td>                               
                                                </tr>
                                            </tfoot>                                            
                                        </table>                            
                                    </div>
                                    <div class="tab-pane" id="tab-payments">
                                                            <table class="table table-bordered">
                                            <tr>
                                                <th class="col-sm-2">Grand Total</th>
                                                <td class="text-right">{{ $grandTotal }}</td>
                                            </tr>
                                            <tr>
                                                <th>Grand Diskon</th>
                                                <td class="text-right">{{ $detail->diskon }}</td>
                                            </tr>
                                            <tr>
                                                <th>Biaya Lain-lain</th>
                                                <td class="text-right">{{ $biayaLain }}</td>
                                            </tr>
                                            <tr>
                                                <?php $grandTotalStlhDiskon = $grandTotal-$detail->diskon+$biayaLain ?>
                                                <th>Grand Total Setelah Diskon</th>
                                                <td class="text-right">{{ $grandTotalStlhDiskon }}</td>
                                            </tr> 
                                            <tr>
                                                <th>Jatuh Tempo</th>
                                                <td class="text-right">{{ CarbonDate($detail->jatuh_tempo, 'd-m-Y') }}</td>
                                            </tr>                       
                                        </table>                    
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-left">Tanggal Pembayaran</th>
                                                    <th class="text-left">Metode</th>
                                                    <th class="text-left">Refrensi</th>
                                                    <th class="text-right">Jumlah</th>                      
                                                </tr>                           
                                            </thead>
                                            <tbody>      
                                                <?php $totalPembayaran = 0 ?>
                                                @foreach($detail->beliBayar()->get() as $beliBayar)                                                  
                                                <tr>
                                                    <td class="text-left"></td>
                                                    <td class="text-left">{{ $beliBayar->metode }}</td>
                                                    <td class="text-left">{{ $beliBayar->ref }}</td>
                                                    <td class="text-right">{{ $beliBayar->jumlah }}</td>    
                                                    <?php 
                                                        $totalPembayaran += $beliBayar->jumlah
                                                     ?>                          
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-left" colspan="3">Total Pembayaran</th>                             
                                                    <td class="text-right">{{ $totalPembayaran }}</td>                              
                                                </tr>
                                            </tfoot>                                                
                                        </table>    
                                        <table class="table table-bordered">
                                            <tr>
                                                <th class="col-sm-2">Selisih Pembayaran</th>
                                                <td class="text-right">{{ $grandTotalStlhDiskon-$totalPembayaran }}</td>
                                            </tr>                                           
                                        </table>                               
                                    </div>
                                    <div class="tab-pane" id="tab-detail">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th class="col-sm-2">Tanggal Pembelian</th>
                                                <td>{{ $detail->tanggal }}</td>
                                            </tr>
                                            <tr>
                                                <th>No Faktur</th>
                                                <td>{{ $detail->no_faktur }}</td>
                                            </tr>
                                            <tr>
                                                <th>Status Pembelian</th>
                                                <td>
                                                    <?php 
                                                        if ($detail->status == 0) {
                                                            echo "Belum Lunas";
                                                        }else{
                                                            echo "Lunas";
                                                        }
                                                     ?>
                                                </td>
                                            </tr>                                       
                                        </table>                    
                                    </div>
                                </div>          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
        });
        function getFaktur() {
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = 'http://localhost:8000/hutang/getFaktur/'+faktur;
            $.ajax({
                url:base_url,
                type:'get',
                dataType:'json',
                data:'faktur='+faktur,
                success:function(response) {
                    if (response['err']) {
                        alert('Data tidak ditemukan.');
                    }else{
                        alert('Data sudah ditemukan.');
                        $('input[name="id_beli"').val(response);
                    }
                }
            });
        };
    </script>
@endpush