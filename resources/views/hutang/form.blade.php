<div class="form-group{{ $errors->has('id_beli') ? ' has-error' : '' }}">
  {!! Form::label('id_beli', 'No Faktur', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::hidden('id_beli', null, ['class'=>'form-control']) !!}
      {!! Form::hidden('total_debit', null, ['class'=>'form-control']) !!}
      {!! Form::hidden('total_credit', null, ['class'=>'form-control']) !!}
      {!! Form::text('no_faktur', null, ['class'=>'form-control', 'onkeyup'=>'getData()', 'onchange'=>'getData()']) !!}
      <span class="input-group-btn">
        <a class="btn btn-primary" id="detail" onclick="detail()"><i class="fa fa-search"></i></a>
        <a class="btn btn-success" onclick="showDetail()"><i class="fa fa-book"></i></a>
      </span>
    </div>
    {!! $errors->first('id_beli', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
  {!! Form::label('tanggal', 'Tanggal', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::text('tanggal', date('Y-m-d'), ['class'=>'form-control datepicker']) !!}
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
      </span>
    </div>
    {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('metode') ? ' has-error' : '' }}">
  {!! Form::label('metode', 'Metode', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('metode', [
      'cash'=>'Cash',
      'transfer'=>'Transfer',
      'edc'=>'EDC',
      'giro'=>'Giro',
      'check'=>'Check'
    ],null, ['class'=>'form-control']) !!}
    {!! $errors->first('metode', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('ref') ? ' has-error' : '' }}">
  {!! Form::label('ref', 'Referensi', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('ref', null, ['class'=>'form-control']) !!}
    {!! $errors->first('ref', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('jumlah') ? ' has-error' : '' }}">
  {!! Form::label('jumlah', 'Jumlah', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('jumlah', null, ['class'=>'form-control text-right']) !!}
    {!! $errors->first('jumlah', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
  {!! Form::label('status', 'Status', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('status', [
      '0'=>'Belum Lunas',
      '1'=>'Lunas'
    ],null, ['class'=>'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
  </div>
</div>