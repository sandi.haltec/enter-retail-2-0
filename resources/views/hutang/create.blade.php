@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Tambah Hutang</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            {!! Form::open(['route' => 'hutang.store', 'class' => 'form-horizontal']) !!}
                                @include('hutang.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ url('hutang') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetail" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Riwayat Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="payments" class="table table-bordered">
                        <tr>
                            <th class="text-center" colspan="4">Keterangan</th>                                                                                                                                                      
                            <th class="text-center" rowspan="2">Debet</th>
                            <th class="text-center" rowspan="2">Kredit</th>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-center">Penanggung Jawab</th>                                                                                                 
                            <th class="text-center">Tanggal Pembayaran</th>
                            <th class="text-center">Metode</th>
                            <th class="text-center">Referensi</th>                                                                                                                                                        
                        </tr>
                        <tbody id="info_pay">

                        </tbody>
                        <tr>
                            <th class="text-left" colspan="4">Grand Total</th>                                                                                                                                                      
                            <td class="text-center">&nbsp;</td>
                            <td class="text-right" id="item_total">0</td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Biaya Lain-lain</th>                                                                                                                                                      
                            <td class="text-center">&nbsp;</td>         
                            <td class="text-right" id="other_total">0</td>

                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Diskon</th>                                                                                                                                                      
                            <td class="text-right" id="discount"></td>                                                         
                            <td class="text-center">&nbsp;</td>
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Debet | Kredit</th>                                                                                                                                                      
                            <td class="text-right" id="total_debit">0</td>
                            <td class="text-right" id="total_credit">0</td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Selisih Debet & Kredit</th>                                                                                                                                                                                                                                                                      
                            <td class="text-right" id="due" colspan="2">0</td>                                                                                                                                                                                                                                                                      
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
        });

        function showDetail() {
            $('#modalDetail').modal('show');
        }

        function getData() {
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = '<?php echo url("hutang/getFaktur/") ?>'+'/'+faktur;
            $.ajax({
                url:base_url,
                type:'get',
                dataType:'json',
                data:'faktur='+faktur,
                success:function(response) {
                    if (response['err']) {
                        $('#info_pay').empty();
                        $('#other_total').html(response['other_total']);
                        $('#item_total').html(response['item_total']);
                        $('#total_debit').html(response['total_debit']);
                        $('#total_credit').html(response['total_credit']);
                        $('#due').html(response['due']);
                        $('input[name="id_penjualan"]').val('');
                        $('#detail').addClass('disabled');
                        return true;
                    }else{
                        if(response['metode'] == 1){
                            var metode = 'Cash';
                        }else if(response['metode'] == 2){
                            var metode = 'Transfer';
                        }else if(response['metode'] == 3){
                            var metode = 'EDC';
                        }else if(response['metode'] == 4){
                            var metode = 'Giro';
                        }else{
                            var metode = 'Check';
                        }

                        var html = null;
                        var totalPembayaran = 0;
                        $.each(response['beliBayar'], function(index, obj){
                            html += '<tr>'+
                                '<td>'+obj.author+'</td>'+
                                '<td>'+obj.tanggal+'</td>'+
                                '<td>'+obj.metode+'</td>'+
                                '<td>'+obj.ref+'</td>'+
                                '<td class="text-right">'+obj.jumlah+'</td>'+
                                '<td></td>'+
                            '</tr>';
                            totalPembayaran += obj.jumlah;
                        });
                        $('#info_pay').html(html);

                        $('#other_total').html(response['biayaLain']);
                        $('#discount').html(response['diskon']);
                        $('#item_total').html(response['grandTotal']);

                        var total_debit = totalPembayaran-parseInt(response['diskon']);
                        $('#total_debit').html(total_debit);
                        var total_credit = parseInt(response['grandTotal'])+parseInt(response['biayaLain']);
                        $('#total_credit').html(total_credit);

                        $('#due').html(parseInt(total_credit)-parseInt(total_debit));
                    
                        $('input[name="id_beli"]').val(response['id_beli']);
                        $('input[name="total_debit"]').val(total_debit);
                        $('input[name="total_credit"]').val(total_credit);
                        $('#detail').removeClass('disabled');
                    }
                }
            });
        }

        function tampil(){
            alert('asd');
        }

        function detail(){
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = '<?php echo url("hutang/detail/") ?>'+'/'+faktur;
            if(faktur!=''){
                $url = document.location.href=base_url;
            }
        }
    </script>
@endpush