@if ($errors->any()) 
    <div class="alert alert-danger"> 
        <strong>Terjadi Kesalahan Input! </strong> 
        <ul> 
            @foreach ($errors->all() as $error) 
                <li>{{ $error }}</li> 
            @endforeach 
        </ul> 
    </div> 
@endif
@include('flash::message')
