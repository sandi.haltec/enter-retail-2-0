<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('barang.index') }}">Barang</a></li>
                    <li><a href="{{ route('kategori.index') }}">Kategori</a></li>
                    <li><a href="{{ route('satuan.index') }}">Satuan</a></li>
                    {{-- <li><a href="{{ route('satuan.buku_harga') }}">Buku Harga</a></li> --}}
                    <li><a href="{{ route('pelanggan.index') }}">Pelanggan</a></li>
                    <li><a href="{{ route('supplier.index') }}">Supplier</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>