<div class="form-group">
  {!! Form::label('nama_supplier', 'Supplier *', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('nama_supplier', null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('alamat', 'Alamat *', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::textarea('alamat', null, ['class'=>'form-control', 'rows' => 3]) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('no_telp', 'Nomor Telepon *', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('no_telp', null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('email', 'Email *', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('email', null, ['class'=>'form-control']) !!}
  </div>
</div>