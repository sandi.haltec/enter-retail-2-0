@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('supplier/create') }}" data-toggle="tooltip" title="Tambah Data" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span></a>
                    <a href="{{ url('supplier/export') }}" data-toggle="tooltip" title="Unduh Data" class="btn btn-sm btn-success"><i class="fa fa-download"></i></a>
                    <button onclick="showImportModal()" data-toggle="tooltip" title="Unggah Data" class="btn btn-sm btn-success"><i class="fa fa-upload"></i></button>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Supplier</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Nama Supplier</td>
                                        <td>Alamat</td>
                                        <td>Nomor Telepon</td>
                                        <td>Email</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="importModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            {!! Form::open(['route' => 'supplier.import', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Import Data Supplier</h4>
                </div>
                <div class="modal-body">
                    <input type="file" name="excel_file" class="btn btn-default" style="width: 100%; height: 200px;">
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <a href="{{ route('supplier.example') }}" class="btn btn-default"><i class="fa fa-download"></i></a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i></button>
                        <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function() {
            $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/supplier/get',
                columns:[
                    {data: 'nama_supplier', name: 'nama_supplier'},
                    {data: 'alamat', name: 'alamat'},
                    {data: 'no_telp', name: 'no_telp'},
                    {data: 'email', name: 'email'},
                    {data: 'id_supplier', name: 'id_supplier', class: 'text-right', orderable: false, render: function(data, type, row) {
                        html = '<a href="supplier/edit/'+data+'" data-toggle="tooltip" title="Edit Data" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span></a>';
                        html += '<a onclick="confirm(\'supplier/destroy/'+data+'\')" data-toggle="tooltip" title="Hapus Data" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';
                        return html;
                    }},
                ]
            });
        });
        function showImportModal() {
            $('#importModal').modal('show');
        }
    </script>
@endpush