@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('satuan/create') }}" data-toggle="tooltip" title="Tambah Data" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Satuan</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Nama Satuan</td>
                                        <td width="200" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
    $(function() {
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'api/satuan/get',
            columns:[
                {data: 'nama_satuan', name: 'nama_satuan'},
                {data: 'id_satuan', class: 'text-right', orderable: false, name: 'id_satuan', render: function(data, type, row) {
                    html = '<a href="satuan/edit/'+data+'" data-toggle="tooltip" title="Edit Data" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span></a>';
                    html += '<a onclick="confirm()" data-toggle="tooltip" title="Hapus Data" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></a>';
                    return html;
                }},
            ]
        });
    });
    </script>
@endpush