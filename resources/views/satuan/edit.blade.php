@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Ubah Satuan</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            {!! Form::model($satuan, ['route' => ['satuan.update', $satuan->id_satuan], 'class' => 'form-horizontal']) !!}
                                @include('satuan.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ route('satuan.index') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection