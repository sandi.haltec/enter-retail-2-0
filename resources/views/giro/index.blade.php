@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('giro/create') }}" class="btn btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Giro</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <div class="" data-example-id="togglable-tabs">
                            <ul class="nav nav-tabs bar_tabs">
                                <li class="active"><a href="#belumCair" data-toggle="tab" aria-expanded="true">Belum Cair</a>
                                </li>
                                <li class=""><a href="#sudahCair" data-toggle="tab" aria-expanded="false">Sudah Cair</a>
                                </li>
                                <li class=""><a href="#hangus" data-toggle="tab" aria-expanded="false">Hangus</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="belumCair">
                                    <table class="table table-striped belumCair">
                                        <thead>
                                            <tr>
                                                <td>No Referensi</td>
                                                <td>No Faktur</td>
                                                <td>Jatuh Tempo</td>
                                                <td>Jumlah</td>
                                                <td width="150" class="text-right">Aksi</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="sudahCair">
                                    <table class="table table-striped sudahCair">
                                        <thead>
                                            <tr>
                                                <td>No Referensi</td>
                                                <td>No Faktur</td>
                                                <td>Jatuh Tempo</td>
                                                <td>Tanggal Cair</td>
                                                <td>Jumlah</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="hangus">
                                    <table class="table table-striped hangus">
                                        <thead>
                                            <tr>
                                                <td>No Referensi</td>
                                                <td>No Faktur</td>
                                                <td>Jatuh Tempo</td>
                                                <td>Tanggal Hangus</td>
                                                <td>Jumlah</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function(){
            $('.belumCair').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/giro/getBelumCair',
                columns: [
                    {data: 'no_ref', name: 'no_ref'},
                    {data: 'no_faktur', name: 'no_faktur'},
                    {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    {data: 'total', name: 'total'},
                    {data: 'id_giro', name: 'id_giro', render: function(data, type, row) {
                        html = '<a href="giro/hangus/'+data+'" class="btn btn-sm btn-danger pull-right"><span class="fa fa-times"></span></a>';
                        return html += '<a href="giro/cair/'+data+'" class="btn btn-sm btn-success pull-right"><span class="fa fa-check"></span></a>';
                    }},
                ]
            });
            $('.sudahCair').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/giro/getSudahCair',
                columns: [
                    {data: 'no_ref', name: 'no_ref'},
                    {data: 'no_faktur', name: 'no_faktur'},
                    {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    {data: 'tanggal_cair', name: 'tanggal_cair'},
                    {data: 'total', name: 'total'},
                ]
            });
            $('.hangus').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/giro/getHangus',
                columns: [
                    {data: 'no_ref', name: 'no_ref'},
                    {data: 'no_faktur', name: 'no_faktur'},
                    {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    {data: 'tanggal_hangus', name: 'tanggal_hangus'},
                    {data: 'total', name: 'total'},
                ]
            });
        });
    </script>
@endpush