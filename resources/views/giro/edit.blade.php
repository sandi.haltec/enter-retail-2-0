@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Edit Giro</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                            {!! Form::model($giro, ['route' => ['giro.update', $giro->id_giro], 'class' => 'form-horizontal']) !!}
                                @include('giro.form')
                                <div class="col-md-6 col-md-offset-5"> 
                                    <button class="btn btn-success"><i class="fa fa-save"></i></button> 
                                    <a href="{{ route('giro.index') }}" class="btn btn-default"><i class="fa fa-mail-reply"></i></a> 
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection