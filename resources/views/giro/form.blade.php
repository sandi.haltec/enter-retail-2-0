<!-- <div class="form-group{{ $errors->has('id_penjualan') ? ' has-error' : '' }}">
  {!! Form::label('id_penjualan', 'ID Penjualan', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::hidden('id_penjualan', null) !!}
      {!! Form::text('id_penjualan', null, ['class'=>'form-control']) !!}
      <span class="input-group-btn">
        <a class="btn btn-default" onclick="getPenjualan()"><i class="fa fa-search"></i></a>
      </span>
    </div>
    {!! $errors->first('id_penjualan', '<p class="help-block">:message</p>') !!}
  </div>
</div> -->
<div class="form-group{{ $errors->has('no_faktur') ? ' has-error' : '' }}">
  {!! Form::label('no_faktur', 'No Faktur', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::hidden('id_penjualan', null, ['class'=>'form-control']) !!}
      {!! Form::text('no_faktur', null, ['class'=>'form-control', 'onchange'=>'getData()']) !!}
      <span class="input-group-btn">
        <a class="btn btn-default" onclick="showDetail()"><i class="fa fa-search"></i></a>
      </span>
    </div>
    {!! $errors->first('no_faktur', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('no_ref') ? ' has-error' : '' }}">
  {!! Form::label('no_ref', 'No Referensi', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('no_ref', null, ['class'=>'form-control']) !!}
    {!! $errors->first('no_ref', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('jatuh_tempo') ? ' has-error' : '' }}">
  {!! Form::label('jatuh_tempo', 'Jatuh tempo', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group">
      {!! Form::hidden('tanggal_buat', date('Y-m-d'), ['class'=>'form-control datepicker']) !!}
      {!! Form::text('jatuh_tempo', date('Y-m-d'), ['class'=>'form-control datepicker']) !!}
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
      </span>
    </div>
    {!! $errors->first('jatuh_tempo', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('total') ? ' has-error' : '' }}">
  {!! Form::label('total', 'Total', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('total', null, ['class'=>'form-control text-right']) !!}
    {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group{{ $errors->has('jenis') ? ' has-error' : '' }}">
  {!! Form::label('jenis', 'Jenis', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('', 'Giro piutang', ['class'=>'form-control', 'readonly'=>'readonly']) !!}
    {!! $errors->first('jenis', '<p class="help-block">:message</p>') !!}
  </div>
</div>