@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Giro</h2>
                            <div class="clearfix"></div>
                        </div>
                        @include('partials.message')
                        <div class="x_content">
                        	{!! Form::open(['url' => url('giro/store'), 'method' => 'post', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                                @include('giro.form')
                                <div class="col-md-6 col-md-offset-2"> 
                                    <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                    <a href="{{ url('giro') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetail" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Riwayat Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="payments" class="table table-bordered">
                        <tr>
                            <th class="text-center" colspan="4">Keterangan</th>                                                                                                                                                      
                            <th class="text-center" rowspan="2">Debet</th>
                            <th class="text-center" rowspan="2">Kredit</th>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-center">Penanggung Jawab</th>                                                                                                 
                            <th class="text-center">Tanggal Pembayaran</th>
                            <th class="text-center">Metode</th>
                            <th class="text-center">Referensi</th>                                                                                                                                                        
                        </tr>
                        <tbody id="info_pay">

                        </tbody>
                        <tr>
                            <th class="text-left" colspan="4">Grand Total</th>                                                                                                                                                      
                            <td class="text-right" id="item_total">0</td>
                            <td class="text-center">&nbsp;</td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Biaya Lain-lain</th>                                                                                                                                                      
                            <td class="text-right" id="other_total">0</td>
                            <td class="text-center">&nbsp;</td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Diskon</th>                                                                                                                                                      
                            <td class="text-center">&nbsp;</td>
                            <td class="text-right" id="discount"></td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Debet | Kredit</th>                                                                                                                                                      
                            <td class="text-right" id="total_debit">0</td>
                            <td class="text-right" id="total_credit">0</td>                                                                                                   
                        </tr>
                        <tr>
                            <th class="text-left" colspan="4">Selisih Debet & Kredit</th>                                                                                                                                                                                                                                                                      
                            <td class="text-right" id="due" colspan="2">0</td>                                                                                                                                                                                                                                                                      
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
        });
        function getFaktur() {
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = 'http://localhost:8000/giro/getFaktur/'+faktur;
            $.ajax({
                url:base_url,
                type:'get',
                dataType:'json',
                data:'faktur='+faktur,
                success:function(response) {
                    if (response['err']) {
                        swal('Gagal', 'Nomor Faktur tidak ditemukan.', 'error');
                        $('input[name="no_faktur"').val('');
                    }else{
                        swal('Berhasil', 'Nomor Faktur berhasil ditemukan.', 'success');
                        $('input[name="id_beli"').val(response);
                    }
                }
            });
        };
        
        function showDetail() {
            $('#modalDetail').modal('show');
        }

        function getData() {
            var faktur = $('input[name="no_faktur"]').val();
            var base_url = 'http://localhost:8000/giro/getFaktur/'+faktur;
            $.ajax({
                url:base_url,
                type:'get',
                dataType:'json',
                data:'faktur='+faktur,
                success:function(response) {
                    if (response['err']) {
                        $('#info_pay').empty();
                        $('#other_total').html(response['other_total']);
                        $('#item_total').html(response['item_total']);
                        $('#total_debit').html(response['total_debit']);
                        $('#total_credit').html(response['total_credit']);
                        $('#due').html(response['due']);
                        $('input[name="id_penjualan"]').val('');
                    }else{
                        if(response['metode'] == 1){
                            var metode = 'Cash';
                        }else if(response['metode'] == 2){
                            var metode = 'Transfer';
                        }else if(response['metode'] == 3){
                            var metode = 'EDC';
                        }else if(response['metode'] == 4){
                            var metode = 'Giro';
                        }else{
                            var metode = 'Check';
                        }

                        $('#info_pay').html('<tr>'+
                                '<td>'+response["author"]+'</td>'+
                                '<td>'+response["jatuh_tempo"]+'</td>'+
                                '<td>'+metode+'</td>'+
                                '<td>'+response["ref"]+'</td>'+
                                '<td colspan="2"></td>'+
                            '</tr>');
                        $('#other_total').html(response['jual_lain']);
                        $('#item_total').html(response['jumlah']);

                        var total_debit = parseInt(response['jual_lain'])+parseInt(response['jumlah']);
                        $('#total_debit').html(total_debit);
                        $('#total_credit').html(response['jumlah']);

                        var due = parseInt(total_debit)-parseInt(response['jumlah']);
                        $('#due').html(due);
                    
                        $('input[name="id_penjualan"]').val(response['id_jual']);
                    }
                }
            });
        }
    </script>
@endpush