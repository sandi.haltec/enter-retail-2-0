<table id="table-list-beli" class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Barang</th>
            <th width="115">Kadaluarsa</th>
            <th width="80">Jumlah</th>
            <th width="120">Satuan Beli</th>
            <th width="100">Harga</th>
            <th width="80">Potongan</th>
            <th width="140">Total</th>
            <th width="60" class="text-center" colspan="2">PPN (%)</th>
            <th width="50">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr id="list-beli-item">
            <td>
                <div class="input-group">
                    {!! Form::text('nama_barang', null, ['class' => 'form-control']) !!}
                    {!! Form::hidden('id_barang') !!}
                    <span class="input-group-btn">
                        <button onclick="searchBarang()" data-toggle="tooltip" title="Lihat Data Barang" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('expired', CarbonDate(), ['class' => 'form-control date']) !!}
                </div>
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('jumlah', null, ['onkeyup' => 'countTotalItem();checkInputValue()', 'class' => 'form-control text-center']) !!}
                </div>
            </td>
            <td>
                {!! Form::text('satuan_beli', null, ['class' => 'form-control', 'disabled' => true]) !!}
                {!! Form::hidden('id_satuan_beli', null, ['class' => 'form-control']) !!}
            </td>
            <td>
                {!! Form::text('harga_beli', null, ['onkeyup' => 'countTotalItem();checkInputValue()', 'class' => 'form-control text-right input-number']) !!}
            </td>
            <td>
                {!! Form::text('diskon', null, ['onkeyup' => 'countTotalItem();checkInputValue()', 'value' => 0, 'class' => 'form-control input-number']) !!}
            </td>
            <td>
                {!! Form::text('total', null, ['onkeyup' => 'checkInputValue()', 'class' => 'form-control text-right input-number']) !!}
            </td>
            <td width="60">
                {!! Form::text('prosen_ppn', null, ['class' => 'form-control', 'onkeyup' => 'checkInputValue();countTotalItem()']) !!}
            </td>
            <td width="70">
                {!! Form::text('ppn', null, ['class' => 'form-control input-number', 'disabled' => true]) !!}
            </td>
            <td>
                <button type="button" onclick="addList()" data-toggle="tooltip" title="Tambah ke Daftar Dibeli" class="btn btn-primary"><i class="fa fa-plus"></i></button>
            </td>
        </tr>
        @if(isset($beli->beliBarang))
            @foreach($beli->beliBarang as $key => $barang)
                <tr class="list-rows" id="list-row-{{$key}}">
                    <td>
                        {{ $barang->barang->nama_barang }}
                        <input type="hidden" name="barang[{{$key}}][id_barang]" id="list-row-id_barang-{{$key}}" value="{{ $barang->id_barang }}"/>
                        <input type="hidden" name="barang[{{$key}}][nama_barang]" id="list-row-nama_barang-{{$key}}" value="{{ $barang->nama_barang }}"/>
                    </td>
                    <td>
                        {{ CarbonDate($barang->expired) }}
                        <input type="hidden" name="barang[{{$key}}][expired]" id="list-row-expired-{{$key}}" value="{{ $barang->expired }}"/>
                    </td>
                    <td class="text-center">
                        {{ $barang->jumlah }}
                        <input type="hidden" name="barang[{{$key}}][jumlah]" id="list-row-jumlah-{{$key}}" value="{{ $barang->jumlah }}"/>
                    </td>
                    <td>
                        {{ $beli->satuan->nama_satuan }}
                        <input type="hidden" name="barang[{{$key}}][id_satuan_beli]" id="list-row-satuan_beli-{{$key}}" value="{{ $barang->id_satuan_beli }}"/>
                    </td>
                    <td class="text-right">
                        {{ $barang->harga }}
                        <input type="hidden" name="barang[{{$key}}][harga_beli]" id="list-row-harga_beli-{{$key}}" value="{{ $barang->harga }}"/>
                    </td>
                    <td class="text-right">
                        {{ $barang->diskon }}
                        <input type="hidden" name="barang[{{$key}}][diskon]" id="list-row-diskon-{{$key}}" value="{{ $barang->diskon }}"/>
                    </td">
                    <td class="text-right">
                        {{ $barang->total }}
                        <input type="hidden" name="barang[{{$key}}][total]" id="list-row-total-{{$key}}" value="{{ $barang->total }}"/>
                    </td>
                    <td class="text-right">
                        {{ $barang->prosen_ppn }}
                        <input type="hidden" name="barang[{{$key}}][prosen_ppn]" id="list-row-prosen_ppn-{{$key}}" value="{{ $barang->prosen_ppn }}"/>
                    </td>
                    <td class="text-right">
                        {{ $barang->ppn }}
                        <input type="hidden" name="barang[{{$key}}][ppn]" id="list-row-ppn-{{$key}}" value="{{ $barang->ppn }}"/>
                    </td>
                    <td><button type="button" onclick="removeList({{$key}})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<div id="modalSearchBarang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Data Barang</h4>
            </div>
            <div class="modal-body">
                <table id="tableSearchBarang" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" data-toggle="tooltip" title="Tutup" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div id="modalPembayaran" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pembayaran</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs bar_tabs">
                    <li class="active"><a href="#biaya_lain" data-toggle="tab" aria-expanded="true">Biaya Lain-lain</a>
                    </li>
                    <li class=""><a href="#pembayaran" data-toggle="tab" aria-expanded="false">Pembayaran</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="biaya_lain">
                        <table id="table-list-pembayaran-lain" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Keterangan</th>
                                    <th>Harga</th>
                                    <th width="45"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tr-list-pembayaran-lain">
                                    <td>
                                        <input type="text" name="keterangan" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="harga" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" onclick="addListPembayaranLain()" class="btn btn-md btn-primary"><i class="fa fa-plus"></i></button>
                                    </td>
                                </tr>
                                @php $totalBeliLain = 0; @endphp
                                @if(isset($beli->beliLain))
                                    @foreach($beli->beliLain as $key => $belilain)
                                    <tr class="tr-list-pembayaran-lain-row" id="tr-list-pembayaran-lain-row-{{ $key }}">
                                        <td>
                                            {{$belilain->deskripsi}}
                                            <input type="hidden" name="pembayaran_lain[{{ $key }}][keterangan]" value="{{ $belilain->deskripsi }}"/>
                                        </td>
                                        <td>
                                            {{$belilain->harga}}
                                            <input type="hidden" name="pembayaran_lain[{{ $key }}][harga]" value="{{ $belilain->harga }}"/>
                                        </td>
                                        <td>
                                           <button type="button" onclick="deleteListPembayaranLain({{ $key }})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @php $totalBeliLain += $belilain->harga; @endphp
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade active in" id="pembayaran">
                        <div class="form-group">
                            {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('grand_total', isset($beli->total) ? $beli->total : '', ['class' => 'form-control col-md-8 text-right input-number', 'disabled' => 'true']) !!} 
                                {!! Form::hidden('float_grand_total', isset($beli->total) ? $beli->total : '') !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('grand_diskon', 'Grand Diskon', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('grand_diskon', isset($beli->diskon) ? $beli->diskon : '', ['onkeyup' => 'countGrandTotal()', 'class' => 'form-control col-md-8 text-right input-number']) !!} 
                                {!! Form::hidden('float_grand_diskon', isset($beli->diskon) ? $beli->diskon : '') !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('grand_ppn', 'Grand PPN', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('grand_ppn', isset($beli->ppn) ? $beli->ppn : '', ['class' => 'form-control col-md-8 text-right input-number', 'readonly']) !!} 
                                {!! Form::hidden('float_grand_ppn', isset($beli->ppn) ? $beli->ppn : '') !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('biaya_lain', 'Biaya Lain-lain', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('biaya_lain', isset($totalBeliLain) ? $totalBeliLain : 0, ['class' => 'form-control col-md-8 text-right input-number', 'disabled' => true]) !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('grand_total_setelah_diskon', 'Grand Total Setelah Diskon', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('grand_total_setelah_diskon', isset($beli->total) ? $beli->total-$beli->diskon : '', ['class' => 'form-control col-md-8 text-right input-number', 'readonly']) !!} 
                                {!! Form::hidden('float_grand_total_setelah_diskon', isset($beli->total) ? $beli->total-$beli->diskon : '') !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('jatuh_tempo', 'Jatuh Tempo', ['class' => 'control-label col-md-4']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('jatuh_tempo', isset($beli->jatuh_tempo) ? CarbonDate($beli->jatuh_tempo) : CarbonDate(), ['class' => 'form-control col-md-8 date']) !!} 
                            </div> 
                        </div>
                        <table id="table-list-pembayaran" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="120">Tanggal Pembayaran</th>
                                    <th>Metode</th>
                                    <th width="120">Referensi</th>
                                    <th width="100">Jumlah</th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tr-list-pembayaran">
                                    <td width="120">
                                        {!! Form::text('tanggal_pembayaran', CarbonDate(), ['class' => 'form-control date']) !!}
                                    </td>
                                    <td>
                                        {!! Form::select('metode', ['1' => 'Giro', '2' => 'Cash', '3' => 'Kredit'], null,  ['class' => 'form-control']) !!}
                                    </td>
                                    <td width="120">
                                        {!! Form::text('referensi', null, ['class' => 'form-control']) !!}
                                    </td>
                                    <td width="100">
                                        {!! Form::text('jumlah_pembayaran', null, ['class' => 'form-control input-number text-right']) !!}
                                    </td>
                                    <td>
                                        <button type="button" onclick="addListPembayaran()" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                    </td>
                                </tr>
                                @php $totalBayar = 0 @endphp
                                @if(isset($beli->beliBayar))
                                    @foreach($beli->beliBayar as $key => $belibayar)
                                        <tr id="tr-list-pembayaran-row-{{ $key }}" class="tr-list-pembayaran-row">
                                            <td>
                                                {{ $belibayar->tanggal }}
                                                <input type="hidden" name="pembayaran[{{ $key }}][tanggal_pembayaran]" value="{{ $belibayar->tanggal }}">
                                            </td>
                                            <td>
                                                {{ $belibayar->metode }}
                                                <input type="hidden" name="pembayaran[{{ $key }}][metode]" value="{{ $belibayar->metode }}">
                                            </td>
                                            <td>
                                                {{ $belibayar->ref }}
                                                <input type="hidden" name="pembayaran[{{ $key }}][referensi]" value="{{ $belibayar->ref }}">
                                            </td>
                                            <td>
                                                {{ $belibayar->jumlah }}
                                                <input type="hidden" name="pembayaran[{{ $key }}][jumlah]" value="{{ $belibayar->jumlah }}">
                                            </td>
                                            <td>
                                                <button type="button" onclick="deleteListPembayaran({{ $key }})" class="btn btn-md btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @php $totalBayar += $belibayar->jumlah; @endphp
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="form-group">
                            {!! Form::label('total_pembayaran', 'Total Pembayaran', ['class' => 'control-label col-md-4 text-right']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('total_pembayaran', isset($totalBayar) ? $totalBayar : 0, ['class' => 'form-control col-md-8 text-right input-number', 'readonly']) !!} 
                                {!! Form::hidden('float_total_pembayaran', isset($totalBayar) ? $totalBayar : 0) !!} 
                            </div> 
                        </div>
                        <div class="form-group">
                            {!! Form::label('selisih_pembayaran', 'Selisih Pembayaran', ['class' => 'control-label col-md-4 text-right']) !!} 
                            <div class="col-md-8"> 
                                {!! Form::text('selisih_pembayaran', isset($beli->total) ? $totalBayar - $beli->total : '', ['class' => 'form-control col-md-8 text-right input-number', 'readonly']) !!} 
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" onclick="submit()" class="btn btn-primary"><i class="fa fa-save"></i></a>
            </div>
        </div>
    </div>
</div>