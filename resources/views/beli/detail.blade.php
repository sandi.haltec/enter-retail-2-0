@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Detail Pembelian</h3>
                </div>
                <div class="title_right">
                    <div class="pull-right">
                        <a href="{{ route('beli.index') }}" class="btn btn-md btn-default"><i class="fa fa-mail-reply"></i></a>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        @include('partials.message')
                        <div class="x_content">
                            <div class="form-horizontal">
                                <div class="" data-example-id="togglable-tabs">
                                    <ul class="nav nav-tabs bar_tabs">
                                        <li class="active"><a href="#pembelian" data-toggle="tab" aria-expanded="true">Pembelian</a></li>
                                        <li class=""><a href="#barang" data-toggle="tab" aria-expanded="false">Barang</a></li>
                                        <li class=""><a href="#supplier" data-toggle="tab" aria-expanded="false">Supplier</a></li>
                                        <li class=""><a href="#total" data-toggle="tab" aria-expanded="false">Total</a></li>
                                        <li class=""><a href="#pembayaran" data-toggle="tab" aria-expanded="false">Pembayaran</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="pembelian">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('tanggal', 'Tanggal Pembelian', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('tanggal', CarbonDate($beli->tanggal), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('no_faktur', 'No Faktur', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('no_faktur', $beli->no_faktur, ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('status', 'Status Pembelian', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::select('status', ['1' => 'Lunas', '2' => 'Belum Lunas'], $beli->status, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="barang">
                                            <table class="table table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Kode Barang</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga</th>
                                                        <th>Jumlah</th>
                                                        <th>Diskon</th>
                                                        <th>PPN</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($beli->beliBarang as $value)
                                                        <tr>
                                                            <td>{{ $value->barang->kode_barang }}</td>
                                                            <td>{{ $value->barang->nama_barang }}</td>
                                                            <td>{{ numberToPrice($value->harga) }}</td>
                                                            <td>{{ $value->jumlah }}</td>
                                                            <td>{{ numberToPrice($value->diskon) }}</td>
                                                            <td>{{ numberToPrice($value->ppn) }}</td>
                                                            <td>{{ numberToPrice($value->total) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="supplier">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('supplier', 'Supplier', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('supplier', $beli->supplier->nama_supplier, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('no_telp', 'No Telp', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('no_telp', $beli->supplier->no_telp, ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('email', 'Email', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('email', $beli->supplier->email, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                                <div class="form-group"> 
                                                    {!! Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::textarea('alamat', $beli->supplier->alamat, ['class' => 'form-control col-md-8', 'rows' => '3', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="total">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('total_diskon', 'Total Diskon', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('total_diskon', numberToPrice($beli->diskon), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('total_ppn', 'Total PPN', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('total_ppn', numberToPrice($beli->ppn), ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('grand_total', numberToPrice($beli->total), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="pembayaran">
                                            <table class="table table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Tanggal Bayar</th>
                                                        <th>Metode</th>
                                                        <th>Referensi</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($beli->beliBayar as $value)
                                                        <tr>
                                                            <td>{{ $value->tanggal }}</td>
                                                            <td>{{ $value->metode }}</td>
                                                            <td>{{ $value->ref }}</td>
                                                            <td>{{ numberToPrice($value->jumlah) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/jquery-ui/css/jquery-ui.css') }}">
    <link rel="stylesheet" src="{{ asset('vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/jquery-ui/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendors/jquery-number/jquery.number.min.js') }}"></script>
    <script src="{{ asset('app/beli/js/form_beli.js') }}"></script>
@endpush