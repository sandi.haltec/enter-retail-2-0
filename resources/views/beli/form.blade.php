<div class="col-md-6">
    <div class="form-group"> 
    {!! Form::label('tanggal', 'Tanggal Pembelian *', ['class' => 'control-label col-md-4']) !!} 
        <div class="col-md-8"> 
            {!! Form::text('tanggal', isset($beli->tanggal) ? $beli->tanggal : CarbonDate(), ['class' => 'form-control col-md-8 date']) !!} 
        </div> 
    </div>
    <div class="form-group"> 
        {!! Form::label('no_faktur', 'No Faktur *', ['class' => 'control-label col-md-4']) !!} 
        <div class="col-md-8"> 
            {!! Form::text('no_faktur', isset($beli->no_faktur) ? $beli->no_faktur : '', ['class' => 'form-control col-md-8']) !!} 
            {!! Form::hidden('id_beli', isset($beli->id_beli) ? $beli->id_beli : '') !!} 
        </div> 
    </div> 
</div>
<div class="col-md-6"> 
    <div class="form-group"> 
    {!! Form::label('nama', 'Supplier *', ['class' => 'control-label col-md-4']) !!} 
        <div class="col-md-8"> 
            <div class="input-group">
                {!! Form::text('nama_supplier', isset($beli->supplier->nama_supplier) ? $beli->supplier->nama_supplier : '', ['class' => 'form-control col-md-9']) !!} 
                {!! Form::hidden('id_supplier', isset($beli->supplier->id_supplier) ? $beli->supplier->id_supplier : '') !!} 
                <span class="input-group-btn">
                   <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Lihat Data Supplier" onkeyup="checkSupplier()" onclick="searchSupplier()"><i class="fa fa-search"></i></button> 
                </span>
            </div>
        </div> 
    </div>
    <div class="form-group"> 
        {!! Form::label('status', 'Status Pembelian', ['class' => 'control-label col-md-4']) !!} 
        <div class="col-md-8"> 
            {!! Form::select('status', ['1' => 'Lunas', '2' => 'Belum Lunas'], isset($beli->status) ? $beli->status : '', ['class' => 'form-control col-md-8']) !!} 
        </div> 
    </div>
</div> 
<div id="modalSearchSupplier" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Data Supplier</h4>
            </div>
            <div class="modal-body">
                <table id="tableSearchSupplier" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nama Supplier</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>