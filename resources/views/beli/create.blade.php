@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3></h3>
                </div>
                <div class="title_right">
                    <div class="pull-right">
                        <a href="{{ route('beli.index') }}" data-toggle="tooltip" title="Kembali" class="btn btn-md btn-default"><i class="fa fa-mail-reply"></i></a>
                        <button onclick="showModalPembayaran()" data-toggle="tooltip" title="Bayar" class="btn btn-md btn-primary"><i class="fa fa-save"></i></button>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                {!! Form::open(['route' => 'beli.store', 'id' => 'form-pembelian']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Form Pembelian</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="message">
                                
                            </div>
                            <div class="x_content">
                                <div class="form-horizontal">
                                    @include('beli.form')
                                </div>
                            </div>
                            <div class="x_content">
                                <div class="form-horizontal">
                                    @include('beli.form_beli')
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/jquery-ui/css/jquery-ui.css') }}">
    <link rel="stylesheet" src="{{ asset('vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/jquery-ui/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendors/jquery-number/jquery.number.min.js') }}"></script>
    <script src="{{ asset('app/beli/js/form_beli.js') }}"></script>
@endpush