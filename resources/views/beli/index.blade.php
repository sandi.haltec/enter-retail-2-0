@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Pembelian</h3>
                </div>
                <div class="title_right">
                    <div class="pull-right">
                        <a href="{{ route('beli.create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Pembelian</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('partials.message')
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>No Faktur</td>
                                        <td>Tanggal</td>
                                        <td>Supplier</td>
                                        <td>Total</td>
                                        <td width="120">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
    $(function() {
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'api/pembelian/get',
            columns:[
                {data: 'no_faktur'},
                {data: 'tanggal'},
                {data: 'supplier.nama_supplier'},   
                {data: 'total'},
                {data: 'id_beli', orderable: false, render: function(data, type, row) {
                    html = '<a href="pembelian/detail/'+data+'" class="btn btn-info btn-sm"><i class="fa fa-info"></i></a>';
                    if(row['tangguhkan'] == 1) {
                        html += '<a href="pembelian/edit/'+data+'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>';
                        html += '<a onclick="confirm(\'pembelian/destroy/'+data+'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
                    } else if(row['tangguhkan'] == 0) {
                        html += '<a href="pembelian/tangguhkan/'+data+'" class="btn btn-warning btn-sm"><i class="fa fa-times"></i></a>';
                    }
                    return html;
                }}   
            ]
        });
    });
    function showImportModal() {
        $('#importModal').modal('show');
    }
    </script>
@endpush