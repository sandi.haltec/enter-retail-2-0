@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('penjualan/create') }}" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Penjualan</h2>
                            <div class="clearfix"></div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label" for="input-start_date">Tanggal Awal</label>
                                            <div class="input-group date">
                                                <input type="text" name="start_date" placeholder="Tanggal Awal" class="form-control datepicker" onchange="loadData()">
                                                <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label" for="input-end_date">Tanggal Akhir</label>
                                            <div class="input-group date">
                                                <input type="text" name="end_date" placeholder="Tanggal Akhir" class="form-control datepicker" onchange="loadData()">
                                                <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-3 col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label" for="status">Status</label>
                                            <select class="form-control" name="status" id="status" onchange="loadData()">
                                                <option value="1">Belum Lunas</option>
                                                <option value="2">Lunas</option>
                                            </select>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>Pelanggan</td>
                                        <td>Jatuh Tempo</td>
                                        <td>Total</td>
                                        <td>Status</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var tbl = $('table').dataTable({
            processing: true,
            serverSide: true,
            ajax: 'api/jual/get',
            columns:[
                {data: 'tanggal', name: 'tanggal'},
                {data: 'pelanggan.nama_pelanggan', name: 'pelanggan.nama_pelanggan'},
                {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                {data: 'total', name: 'total'},
                {data: 'status', name: 'status'},
                {data: 'id_jual', name: 'id_jual', class: 'text-right', render: function(data, type, row) {
                    html = '<a href="penjualan/detail/'+data+'" class="btn btn-sm btn-info"><span class="fa fa-info"></span></a>';
                    return html += '<a onclick="confirm(\'penjualan/destroy/'+data+'\')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';
                }},
            ]
        });

        function loadData() {
            var start_date = $('input[name="start_date"]').val();
            var end_date = $('input[name="end_date"]').val();
            var status = $('select[name="status"] option:selected').val();
            tbl.api().ajax.url('api/jual/get?start_date='+start_date+'&end_date='+end_date+'&status='+status).load();
        }
        
        $('.datepicker').datepicker({
            format:'yyyy-mm-dd',
            autoclose:true
        });
    </script>
@endpush