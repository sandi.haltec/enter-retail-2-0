@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Penjualan</h3>
                </div>
                <div class="title_right">
                    <div class="pull-right">
                        <button onclick="showModalPembayaran()" class="btn btn-md btn-primary"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                {!! Form::model($jual, ['route' => ['jual.update', $jual->id_jual], 'class' => 'form-horizontal']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Detail</h2>
                                <div class="clearfix"></div>
                            </div>
                            @include('partials.message')
                            <div class="x_content">
                                <div class="form-horizontal">
                                    @include('jual.form')
                                    <div class="col-md-6 col-md-offset-2"> 
                                        <button class="btn btn-success"><span class="fa fa-save"></span></button> 
                                        <a href="{{ url('jual') }}" class="btn btn-default"><span class="fa fa-mail-reply"></span></a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Pembelian</h2>
                                <div class="clearfix"></div>
                            </div>
                            @include('partials.message')
                            <div class="x_content">
                                <div class="form-horizontal">
                                    <!-- @include('beli.form_beli') -->
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/jquery-ui/css/jquery-ui.css') }}">
    <link rel="stylesheet" src="{{ asset('vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/jquery-ui/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendors/jquery-number/jquery.number.min.js') }}"></script>
    <script src="{{ asset('app/beli/js/form_beli.js') }}"></script>
@endpush