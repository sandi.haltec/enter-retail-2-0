@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Detail Penjualan</h3>
                </div>
                <div class="title_right">
                    <div class="pull-right">
                        <a href="{{ route('jual.index') }}" class="btn btn-md btn-default"><i class="fa fa-mail-reply"></i></a>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        @include('partials.message')
                        <div class="x_content">
                            <div class="form-horizontal">
                                <div class="" data-example-id="togglable-tabs">
                                    <ul class="nav nav-tabs bar_tabs">
                                        <li class="active"><a href="#penjualan" data-toggle="tab" aria-expanded="true">Penjualan</a></li>
                                        <li class=""><a href="#barang" data-toggle="tab" aria-expanded="false">Barang</a></li>
                                        <li class=""><a href="#pelanggan" data-toggle="tab" aria-expanded="false">Pelanggan</a></li>
                                        <li class=""><a href="#total" data-toggle="tab" aria-expanded="false">Total</a></li>
                                        <li class=""><a href="#pembayaran" data-toggle="tab" aria-expanded="false">Pembayaran</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="penjualan">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('tanggal', 'Tanggal Penjualan', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('tanggal', CarbonDate($jual->tanggal), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('no_faktur', 'No Faktur', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('no_faktur', $jual->no_faktur, ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('status', 'Status Penjualan', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::select('status', ['1' => 'Lunas', '2' => 'Belum Lunas'], $jual->status, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="barang">
                                            <table class="table table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Kode Barang</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga</th>
                                                        <th>Jumlah</th>
                                                        <th>Diskon</th>
                                                        <th>PPN</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($jual->jualBarang as $value)
                                                        <tr>
                                                            <td>{{ $value->barang->kode_barang }}</td>
                                                            <td>{{ $value->barang->nama_barang }}</td>
                                                            <td>{{ numberToPrice($value->harga) }}</td>
                                                            <td>{{ $value->jumlah }}</td>
                                                            <td>{{ numberToPrice($value->diskon) }}</td>
                                                            <td>{{ numberToPrice($value->ppn) }}</td>
                                                            <td>{{ numberToPrice($value->total) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="pelanggan">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('pelanggan', 'Pelanggan', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('pelanggan', $jual->pelanggan->nama_pelanggan, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('no_telp', 'No Telp', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('no_telp', $jual->pelanggan->no_telp, ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('email', 'Email', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('email', $jual->pelanggan->email, ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                                <div class="form-group"> 
                                                    {!! Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::textarea('alamat', $jual->pelanggan->alamat, ['class' => 'form-control col-md-8', 'rows' => '3', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="total">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                {!! Form::label('total_diskon', 'Total Diskon', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('total_diskon', priceToNumber($jual->diskon), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('total_ppn', 'Total PPN', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('total_ppn', priceToNumber($jual->ppn), ['class' => 'form-control col-md-8', 'disabled' => true]) !!} 
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label col-md-4']) !!} 
                                                    <div class="col-md-8"> 
                                                        {!! Form::text('grand_total', priceToNumber($jual->total), ['class' => 'form-control col-md-8', 'readonly']) !!} 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="pembayaran">
                                            <table class="table table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Tanggal Bayar</th>
                                                        <th>Metode</th>
                                                        <th>Referensi</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($jual->jualBayar as $value)
                                                        <tr>
                                                            <td>{{ $value->tanggal }}</td>
                                                            <td>{{ $value->metode }}</td>
                                                            <td>{{ $value->referensi }}</td>
                                                            <td>{{ priceToNumber($value->jumlah) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/jquery-ui/css/jquery-ui.css') }}">
    <link rel="stylesheet" src="{{ asset('vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/jquery-ui/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendors/jquery-number/jquery.number.min.js') }}"></script>
@endpush