<div class="col-md-6">  
  <div class="form-group{{ $errors->has('id_pelanggan') ? ' has-error' : '' }}">
    {!! Form::label('id_pelanggan', 'Pelanggan', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      {!! Form::select('id_pelanggan', [''=>'Pilih Pelanggan']+App\Model\Pelanggan::pluck('nama_pelanggan', 'id_pelanggan')->all(), null, ['class'=>'form-control select2']) !!}
      {!! $errors->first('id_pelanggan', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="form-group{{ $errors->has('id_bank') ? ' has-error' : '' }}">
    {!! Form::label('id_bank', 'Bank', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      {!! Form::select('id_bank', [''=>'Pilih Bank', '1'=>'BRI', '2'=>'Mandiri', '3'=>'BNI'], null, ['class'=>'form-control select2']) !!}
      {!! $errors->first('id_bank', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="form-group{{ $errors->has('ref') ? ' has-error' : '' }}">
    {!! Form::label('ref', 'Ref', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      {!! Form::text('ref', null, ['class'=>'form-control']) !!}
      {!! $errors->first('ref', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
    {!! Form::label('tanggal', 'Tanggal Penjualan', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      <div class="input-group">
        {!! Form::text('tanggal', null, ['class'=>'form-control datepicker']) !!}
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
      {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="form-group{{ $errors->has('jatuh_tempo') ? ' has-error' : '' }}">
    {!! Form::label('jatuh_tempo', 'Jatuh tempo', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      <div class="input-group">
        {!! Form::text('jatuh_tempo', null, ['class'=>'form-control datepicker']) !!}
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
      {!! $errors->first('jatuh_tempo', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="form-group{{ $errors->has('diskon') ? ' has-error' : '' }}">
    {!! Form::label('diskon', 'Diskon', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      {!! Form::text('diskon', null, ['class'=>'form-control']) !!}
      {!! $errors->first('diskon', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="form-group{{ $errors->has('tipe') ? ' has-error' : '' }}">
    {!! Form::label('tipe', 'Tipe', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-8">
      {!! Form::text('tipe', null, ['class'=>'form-control']) !!}
      {!! $errors->first('tipe', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div> 
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();
        $('.datepicker').datepicker({
            format:'yyyy-mm-dd'
        });
    </script>
@endpush