@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ url('kategori/create') }}" data-toggle="tooltip" title="Tambah Data" class="btn btn-primary"><span class="fa fa-plus"></span></a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Kategori</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <td>Nama Kategori</td>
                                        <td width="150" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function(){
            $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: 'api/kategori/get',
                columns: [
                    {data: 'nama_kategori', name: 'nama_kategori'},
                    {data: 'id_kategori', class: 'text-right', orderable: false, name: 'id_kategori', render: function(data, type, row) {
                        html = '<a href="kategori/edit/'+data+'" data-toggle="tooltip" title="Edit Data" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span></a>';
                        return html += '<a onclick="confirm(\'kategori/destroy/'+data+'\')" data-toggle="tooltip" title="Hapus Data" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';
                    }},
                ]
            });
        });
    </script>
@endpush