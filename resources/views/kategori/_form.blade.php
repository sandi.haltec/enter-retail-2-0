<div class="form-group">
  {!! Form::label('nama_kategori', 'Kategori *', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('nama_kategori', null, ['class'=>'form-control']) !!}
  </div>
</div>