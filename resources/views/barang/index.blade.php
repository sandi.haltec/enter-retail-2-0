@extends('layouts.main')
@section('container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="pull-right">
                    <a href="{{ route('barang.create') }}" data-toggle="tooltip" title="Tambah Data" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                    <a href="{{ route('barang.export') }}" data-toggle="tooltip" title="Unduh Data" class="btn btn-sm btn-success"><i class="fa fa-download"></i></a>
                    <button onclick="showImportModal()" data-toggle="tooltip" title="Unggah Data" class="btn btn-sm btn-success"><i class="fa fa-upload"></i></button>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Barang</h2>
                            @include('partials.message')
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Kode Barang</td>
                                        <td>Nama Barang</td>
                                        <td width="100" class="text-right">Aksi</td>
                                    </tr>
                                </thead>
                            </table>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="importModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            {!! Form::open(['route' => 'barang.import', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Import Data Barang</h4>
                </div>
                <div class="modal-body">
                    <input type="file" name="excel_file" class="btn btn-default" style="width: 100%; height: 200px;">
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <a href="{{ route('barang.example') }}" class="btn btn-default"><i class="fa fa-download"></i></a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i></button>
                        <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
    $(function() {
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'api/barang/get',
            columns:[
                {data: 'nama_barang', name: 'nama_barang'},
                {data: 'kode_barang', name: 'kode_barang'},
                {data: 'id_barang', orderable : false, name: 'id_barang', class: 'text-right', render: function(data, type, row) {
                    html = '<a href="barang/edit/'+data+'" data-toggle="tooltip" title="Ubah Data" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>';
                    return html += '<a onclick="confirm(\'barang/destroy/'+data+'\')" data-toggle="tooltip" title="Hapus Data" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';
                }},
            ]
        });
    });
    function showImportModal() {
        $('#importModal').modal('show');
    }
    </script>
@endpush