<div class="" data-example-id="togglable-tabs">
    <ul class="nav nav-tabs bar_tabs">
        <li class="active"><a href="#barang" data-toggle="tab" aria-expanded="true">Barang</a>
        </li>
        <li class=""><a href="#satuanharga" data-toggle="tab" aria-expanded="false">Satuan Harga</a>
        </li>
        <li class=""><a href="#stok" data-toggle="tab" aria-expanded="false">Stok</a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="barang">
            <div class="form-group"> 
            {!! Form::label('kode_barang', 'Kode Barang', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-5"> 
                    {!! Form::text('kode_barang', null, ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div> 
            <div class="form-group"> 
                {!! Form::label('nama_barang', 'Nama Barang', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-6"> 
                    {!! Form::text('nama_barang', null, ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div> 
            <div class="form-group"> 
                {!! Form::label('barcode', 'Barcode', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-6"> 
                    {!! Form::text('barcode', null, ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('ppn', 'PPN', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-2"> 
                    {!! Form::text('ppn', null, ['class' => 'form-control col-md-8']) !!} 
                    <span class="form-control-feedback right">
                        %
                    </span>
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('id_kategori', 'Kategori Barang', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-6"> 
                    {!! Form::select('id_kategori[]', $kategori, isset($kategoriSelected) ? $kategoriSelected : '', ['class' => 'form-control col-md-8 select2', 'multiple']) !!} 
                </div> 
            </div>  
            <div class="form-group"> 
                {!! Form::label('keterangan', 'Keterangan', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-6"> 
                    {!! Form::textarea('keterangan', null, ['rows' => '3', 'class' => 'form-control col-md-8']) !!} 
                </div> 
            </div> 
            <div class="ln_solid"></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="satuanharga">
            <div class="col-md-4"> 
                {!! Form::label('id_satuan_beli', 'Satuan Beli', ['class' => 'control-label']) !!} 
                {!! Form::select('id_satuan_beli', $satuan, isset($satuanHarga->id_satuan_beli) ? $satuanHarga->id_satuan_beli : '', ['class' => 'form-control col-md-8']) !!} 
            </div> 
            <div class="col-md-2"> 
                {!! Form::label('pengali', 'Pengali', ['class' => 'control-label']) !!} 
                {!! Form::text('pengali', isset($satuanHarga->pengali) ? $satuanHarga->pengali : '', ['class' => 'form-control col-md-8', 'onkeyup' => 'hitungHargaJual()']) !!} 
            </div> 
            <div class="col-md-4"> 
                {!! Form::label('id_satuan_jual', 'Satuan Jual', ['class' => 'control-label']) !!} 
                {!! Form::select('id_satuan_jual', $satuan, isset($satuanHarga->id_satuan_jual) ? $satuanHarga->id_satuan_jual : '', ['class' => 'form-control col-md-8']) !!} 
            </div> 
            <div class="col-md-4"> 
                {!! Form::label('harga_beli', 'Harga Satuan Beli', ['class' => 'control-label']) !!} 
                {!! Form::text('harga_beli', isset($satuanHarga->harga_beli) ? $satuanHarga->harga_beli : '', ['onkeyup' => 'hitungHargaJual()', 'class' => 'form-control col-md-8 text-right input-number']) !!} 
            </div> 
            <div class="col-md-3"> 
                {!! Form::label('laba', 'Laba', ['class' => 'control-label']) !!} 
                {!! Form::text('laba', isset($satuanHarga->laba) ? $satuanHarga->laba : '', ['onkeyup' => 'hitungHargaJual()', 'class' => 'form-control col-md-8 text-right input-number']) !!} 
            </div> 
            <div class="col-md-4"> 
                {!! Form::label('harga_jual', 'Harga Satuan Jual', ['class' => 'control-label']) !!} 
                {!! Form::text('harga_jual', isset($satuanHarga->harga_jual) ? $satuanHarga->harga_jual : '', ['class' => 'form-control col-md-8 text-right input-number', 'readonly']) !!} 
            </div> 
            <div class="ln_solid"></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="stok">
            <div class="form-group"> 
                {!! Form::label('stok_masuk', 'Stok Masuk', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-4"> 
                    {!! Form::text('stok_masuk', isset($barangStok->stok_masuk) ? $barangStok->stok_masuk : '', ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('stok_keluar', 'Stok Keluar', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-4"> 
                    {!! Form::text('stok_keluar', isset($barangStok->stok_keluar) ? $barangStok->stok_keluar : '', ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('stok_saat_ini', 'Stok Saat Ini', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-4"> 
                    {!! Form::text('stok_saat_ini', isset($barangStok->stok_saat_ini) ? $barangStok->stok_saat_ini : '', ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('stok_minimal_keluar', 'Stok Minimal Keluar', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-4"> 
                    {!! Form::text('stok_minimal_keluar', isset($barangStok->stok_minimal_keluar) ? $barangStok->stok_minimal_keluar : '', ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="form-group"> 
                {!! Form::label('stok_minimal', 'Stok Minimal', ['class' => 'control-label col-md-2']) !!} 
                <div class="col-md-4"> 
                    {!! Form::text('stok_minimal', isset($barangStok->stok_minimal) ? $barangStok->stok_minimal : '', ['class' => 'form-control col-md-8']) !!} 
                </div> 
            </div>
            <div class="ln_solid"></div>
            <div class="col-md-6 col-md-offset-5"> 
                <button class="btn btn-success"><i class="fa fa-save"></i></button> 
                <a href="{{ route('barang.index') }}" class="btn btn-default"><i class="fa fa-mail-reply"></i></a> 
            </div>
        </div>
    </div>
</div>
@push('css')
    <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('vendors/jquery-number/jquery.number.min.js') }}"></script>
    <script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
    <script>
        function hitungHargaJual() {
            var harga_beli = $("input[name='harga_beli']").val();
            var laba =  $("input[name='laba']").val();
            var pengali =  $("input[name='pengali']").val();
            if(!$.isNumeric(harga_beli)) {
                harga_beli = 0;
            }
            if(!$.isNumeric(laba)) {
                laba = 0;
            }
            if(!$.isNumeric(pengali)) {
                pengali = 0;
            }
            harga_beli = parseFloat(harga_beli);
            laba = parseFloat(laba);
            var harga_jual = (harga_beli / pengali) + laba;
            $("input[name='harga_jual']").val(harga_jual);
        }
        $('.select2').select2();
        $('.input-number').number(true, 2);
    </script>
@endpush