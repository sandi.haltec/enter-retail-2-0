<?php

function getConfig($name) {
    return Illuminate\Support\Facades\Config::get("locale.".$name);
}

function CarbonTime($time = null, $format = null) {
    if (!$time) {
        return Carbon\Carbon::now()->format(($format) ?: getConfig('time'));
    }
    return Carbon\Carbon::parse($time)->format(getConfig('time'));
}

function CarbonDate($date = null, $format = null) {
    if(!$date) {
        return Carbon\Carbon::now()->format(($format) ?: getConfig('date'));
    } 
    return Carbon\Carbon::parse($date)->format(($format) ?: getConfig('date'));
}

function CarbonDateTime($dateTime = null, $format = null) {
    if(!$dateTime) {
        return Carbon\Carbon::now()->format(($format) ?: getConfig('date_time'));
    } 
    return Carbon\Carbon::parse($dateTime)->format(($format) ?: getConfig('date_time'));
}

function CarbonHumanDate($date = null) {
    setlocale(LC_TIME, getConfig('timezone'));
    if(!$date) {
        return strftime(getConfig('human_date'));
    } 
    return strftime(getConfig('human_date'), strtotime(CarbonDate($date)));
}

function CarbonHumanDateTime($dateTime = null) {
    setlocale(LC_TIME, getConfig('timezone'));
    if(!$dateTime) {
        return strftime(getConfig('human_date_time'));
    }
    return strftime(getConfig('human_date_time'), strtotime(CarbonDate($dateTime)));
}
