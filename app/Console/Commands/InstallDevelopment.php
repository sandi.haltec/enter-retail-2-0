<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class InstallDevelopment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install App In Development Mode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting Installation ...');
        if (!File::exists('.env')) {
            $this->error('File Not Found: .env');
            $this->error('Aborted');
            return false;
        }
        $this->info('Generating App key...');
        Artisan::call('key:generate');
        $this->info('Migrating Database...');
        Artisan::call('migrate');
        $this->info('Seeding...');
        Artisan::call('db:seed');
        $this->info('Done!');
    }
}
