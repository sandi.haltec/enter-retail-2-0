<?php 

namespace App\Observers;

use Validator;

class BaseObserver
{
    public function validate($data) {
        $v = Validator::make(request()->all(), $data->rules);
        if($v->fails()) {
            return false;
        }
        return true;
    }

}