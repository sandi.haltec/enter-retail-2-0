<?php 

namespace App\Observers;

use App\Model\Kategori;

class BarangObserver extends BaseObserver
{
    public function creating(Kategori $barang) {
        $this->validate();
    }
}