<?php

namespace App\Http\Controllers;

use DB;
use App\Model\Beli;
use App\Model\Barang;
use App\Model\BeliLain;
use App\Model\BeliBarang;
use App\Model\BarangStok;
use App\Model\BeliBayar;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class BeliController extends Controller
{
    public function index() {
        return view('beli.index');
    }

    public function get() {
        $query = Beli::with('supplier');
        return  Datatables::of($query)->make(true);
    }

    public function detail($id) {
        $beli = Beli::with('beliBarang.barang', 'beliBayar', 'beliLain', 'supplier')
                    ->where('id_beli', $id)
                    ->first();
        return view('beli.detail', compact('beli'));
    }

    private function generateFaktur() {
        $noInvoice = CarbonDate('now', 'dmY');
        $lastInvoice = Beli::where(DB::raw('MID(no_faktur, 3, 6)'), substr($noInvoice, 2, 6))
                           ->max('no_faktur');
        if($lastInvoice) {
            $lastNum = substr($lastInvoice, -5)+1;
            $newInvoice = $noInvoice.substr('00000'.$lastNum, -5);
        } else {
            $newInvoice = $noInvoice.'00001';
        }
        return $newInvoice;
    }

    public function create() {
        $newInvoice = $this->generateFaktur();
        return view('beli.create', compact('newInvoice'));
    }

    public function store(Request $request, Beli $beli) {
        $response = $beli->validation();
        if($response === TRUE) {
            $checkIfExistBeli = Beli::with('beliBarang.barang', 'beliBayar', 'beliLain')
                        ->where('id_beli', $request['id_beli'])
                        ->first();
            if($checkIfExistBeli) {
                $checkIfExistBeli->beliBarang()->delete();
                $checkIfExistBeli->beliLain()->delete();
                $checkIfExistBeli->beliBayar()->delete();
                $checkIfExistBeli->delete();
            }
            $idBeli = $beli->create([
                'no_faktur' => $request['no_faktur'],
                'tanggal' => $request['tanggal'],
                'jatuh_tempo' => $request['jatuh_tempo'],
                'id_supplier' => $request['id_supplier'],
                'total' => $request['grand_total_setelah_diskon'],
                'diskon' => $request['grand_diskon'],
                'ppn' => $request['grand_ppn'],
                'status' => $request['status'],
                'tangguhkan' => false,
                'author' => 'admin'
            ])->id_beli;
            if($request->has('barang')) {
                foreach ($request->barang as $value) {
                    BeliBarang::create([
                        'id_beli' => $idBeli,
                        'id_barang' => $value['id_barang'],
                        'id_satuan_beli' => $value['id_satuan_beli'],
                        'harga' => $value['harga_beli'],
                        'jumlah' => $value['jumlah'],
                        'diskon' => $value['diskon'],
                        'prosen_ppn' => $value['prosen_ppn'],
                        'ppn' => $value['ppn'],
                        'total' => $value['total'],
                        'expired' => $value['expired']
                    ]);
                    $barangStok = BarangStok::find($value['id_barang']);
                    $stokSaatIni = $barangStok->stok_saat_ini;
                    $stokSaatIni += $value['jumlah'];
                    $stokMasuk = $barangStok->stok_masuk;
                    $stokMasuk += $value['jumlah'];
                    $barangStok->update([
                        'stok_saat_ini' => $stokSaatIni,
                        'stok_masuk' => $value['jumlah']
                    ]);
                }
            }
            if($request->has('pembayaran_lain')) {
                foreach ($request->pembayaran_lain as $value) {
                    BeliLain::create([
                        'id_beli' => $idBeli,
                        'harga' => $value['harga'],
                        'deskripsi' => $value['keterangan']
                    ]);
                }
            }
            if($request->has('pembayaran')) {
                foreach ($request->pembayaran as $value) {
                    if($value['jumlah'] == $request['grand_total_setelah_diskon']) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    BeliBayar::create([
                        'id_beli' => $idBeli,
                        'tanggal' => $value['tanggal_pembayaran'],
                        'metode' => $value['metode'],
                        'ref' => $value['referensi'],
                        'jumlah' => $value['jumlah'],
                        'status' => $status,
                        'author' => 'admin'
                    ]);
                }
            }
            return ['success' => true];
        } else {
            return ['error' => true, 'validation' => $response];
        }
    }

    public function edit($id) {
        $beli = Beli::with('beliBarang.barang', 'supplier', 'beliBayar', 'beliLain', 'satuan')->find($id);
        return view('beli.edit', compact('beli'));
    }

    public function tangguhkan($id) {
        $beliBarang = BeliBarang::where('id_beli', $id)->get();
        foreach ($beliBarang as $beli) {
            $barang = Barang::find($beli->id_barang);
            $barangStok = BarangStok::where('kode_barang', $barang->kode_barang)
                                    ->first();
            $barangStok->update([
                'stok_masuk' => $barangStok->stok_masuk - $beli->jumlah,
                'stok_saat_ini' => $barangStok->stok_saat_ini - $beli->jumlah
            ]);
        }
        $beli = Beli::find($id);
        $beli->update(['tangguhkan' => '1']);
        flash('Pembelian berhasil ditangguhkan.')->success();
        return back();
    }

    public function destroy($id) {
        $beli = Beli::find($id);
        $beli->beliBarang()->delete();
        $beli->beliLain()->delete();
        $beli->beliBayar()->delete();
        $beli->delete();
        flash('Pembelian berhasil dihapus.')->success();
        return back();
    }
}
