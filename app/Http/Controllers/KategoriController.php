<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Model\Kategori;
use DB;

class KategoriController extends Controller
{

    public function get()
    {
        return Datatables::of(Kategori::query())->make(true);
    }

    public function index() {
        return view('kategori.index');
    }

    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request, Kategori $kategori)
    {
        $v = $kategori->validate();
        if($v === TRUE) {
            $kategori->create($request->all());
            flash('Data kategori berhasil disimpan.')->success();
            return back();
        }
        return $v;
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    public function update(Request $request, Kategori $kategori, $id)
    {
        $response = $kategori->validate();
        if ($response === TRUE) {
            $kategori->find($id)->update($request->all());
            flash('Data kategori berhasil diperbarui.')->success();
            return back();
        }
        return $response;
    }

    public function destroy(Kategori $kategori, $id)
    {
        $kategori->destroy($id);
        flash('Data kategori berhasil dihapus.')->success();
        return back();
    }
}
