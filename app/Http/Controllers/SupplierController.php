<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\Supplier;

class SupplierController extends Controller
{

    public function index()
    {
        return view('supplier.index');
    }

    public function get() {
        return Datatables::of(Supplier::query())->make(true);
    }

    public function create()
    {
        return view('supplier.create');
    }

    public function store(Request $request, Supplier $supplier)
    {
        $response = $supplier->validate();
        if ($response === TRUE) {
            $supplier->create($request->all());
            flash('Data supplier berhasil disimpan.')->success();
            return back();
        }
        return $response;
    }

    public function edit($id)
    {
        $supplier = Supplier::find($id);
        return view('supplier.edit', compact('supplier'));
    }

    public function update(Request $request, $id, Supplier $supplier)
    {
        $response = $supplier->validate();
        if ($response === TRUE) {
            $supplier->find($id)->update($request->all());
            flash('Data supplier berhasil diperbarui.')->success();
            return back();
        }
        return $response;
    }

    public function destroy($id, Supplier $supplier)
    {
        $supplier->destroy($id);
        flash('Data supplier berhasil dihapus.')->success();
        return back();
    }

    public function export() {
        $supplier = Supplier::get();
        Excel::create('Data Supplier', function($excel) use($supplier) {
            $excel->sheet('SheetName', function($sheet) use($supplier){
                $sheet->cells('A1:D1', function($cells) {
                    $cells->setFontWeight('bold');
                });

                $sheet->fromArray([
                    ['Nama Supplier', 'Alamat', 'No Telp', 'Email']
                ], null, 'A1', true, false);

                foreach ($supplier as $key => $value) {
                    $sheet->fromArray([
                        [$value->nama_supplier, $value->alamat, $value->no_telp, $value->email]
                    ], null, 'A1', false, false);
                }
            });
        })->export('xlsx');
    }

    public function import(Request $request) {
        if ($file = $request->file('excel_file')) {
            $v = Validator::make($request->all(), [
                'excel_file' => 'required|mimes:xlsx,xls'
            ]);
            if($v->fails()) {
                return back()->withErrors($v->errors());
            } else {
                $destinationPath = 'uploads';
                $file->move($destinationPath, $file->getClientOriginalName());
                Excel::load('uploads/'.$file->getClientOriginalName(), function($reader) {
                    $reader->skipRows(0);
                    foreach ($reader->get() as $items) {
                        Supplier::create([
                            'nama_supplier' => $items->nama_supplier,
                            'alamat' => $items->alamat,
                            'no_telp' => $items->no_telp,
                            'email' => $items->email
                        ]);
                    }
                });
                return back();
            }
        }
    }

    public function example() {
        Excel::create('Contoh Import Data Supplier', function($excel) {
            $excel->sheet('SheetName', function($sheet) {
                $sheet->cells('A1:D1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->fromArray([
                    ['Nama Supplier', 'Alamat', 'No Telp', 'Email']
                ], null, 'A1', true, false);
            });
        })->export('xlsx');
    }

    public function find(Request $request) {
        $data = [];
        $post = $request->all();
        return Supplier::find($post['id']);
    }

    public function search(Request $request) {
        $response = [];
        $post = $request->get('term');
        $supplier = Supplier::where('nama_supplier', 'like', '%'.$post.'%')->get();
        foreach ($supplier as $key => $value) {
            $response[] = [
                'label' => $value->nama_supplier . ' - ' . $value->alamat,
                'value' => $value->nama_supplier,
                'id' => $value->id_supplier
            ];
        }
        return json_encode($response);
    }
}
