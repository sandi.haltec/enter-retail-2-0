<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\BeliRetur;

class BeliReturController extends Controller
{

    public function index()
    {
        return view('beli_retur.index');
    }

    public function get() {
        return Datatables::of(BeliRetur::query())->make(true);
    }

    public function create()
    {
        return view('beli_retur.create');
    }

    public function store(Request $request, BeliRetur $beli_retur)
    {
        $response = $beli_retur->validate($request->all());
        if ($response === TRUE) {
            $beli_retur->create($request->all());
            flash('Data Beli retur berhasil disimpan.')->success();
            return redirect('beli_retur');
        }else{
            return back()->withInput()->withErrors($response);
        }
    }

    public function edit($id)
    {
        $beli_retur = BeliRetur::find($id);
        return view('beli_retur.edit', compact('beli_retur'));
    }

    public function update(Request $request, $id, BeliRetur $beli_retur)
    {
        $response = $beli_retur->validate($request->all());
        if ($response === TRUE) {
            $beli_retur->find($id)->update($request->all());
            flash('Data Beli retur berhasil diperbarui.')->success();
            return redirect('beli_retur');
        }else{
            return back()->withInput()->withErrors($response);
        }
    }

    public function destroy($id, BeliRetur $beli_retur)
    {
        $beli_retur->destroy($id);
        flash('Data Beli retur berhasil dihapus.');
        return back();
    }
}
