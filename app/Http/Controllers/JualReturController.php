<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\JualRetur;

class JualReturController extends Controller
{

    public function index()
    {
        return view('jual_retur.index');
    }

    public function get()
    {
        return Datatables::of(JualRetur::query())->make(true);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        $jual_retur = JualRetur::find($id);
        return view('jual_retur.edit', compact('jual_retur'));
    }


    public function update(Request $request, $id, JualRetur $jual_retur)
    {
        $response = $jual_retur->validate();
        if ($response === TRUE) {
            $jual_retur->find($id)->update($request->all());
            flash('Data jual retur berhasil diperbarui.')->success();
            return redirect('jual_retur');
        }
        return $response;
    }

    public function destroy($id, JualRetur $jual_retur)
    {
        $jual_retur->destroy($id);
        flash('Data jual retur berhasil dihapus')->success();
        return back();
    }
}
