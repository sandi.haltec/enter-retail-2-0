<?php

namespace App\Http\Controllers;

use Excel;
use Validator;
use App\Model\Pelanggan;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PelangganController extends Controller
{
    public function index() {
        return view('pelanggan.index');
    }

    public function get() {
        return Datatables::of(Pelanggan::query())->make(true);
    }

    public function create() {
        return view('pelanggan.create');
    }

    public function store(Request $request, Pelanggan $pelanggan) {
        $response = $pelanggan->validate();
        if($response === TRUE) {
            $pelanggan->create($request->all());
            flash('Data pelanggan berhasil disimpan')->success();
            return back();
        } 
        return $response;
    }

    public function edit(Pelanggan $pelanggan, $id) {
        $pelanggan = $pelanggan->find($id);
        return view('pelanggan.edit', compact('pelanggan'));
    }

    public function update(Request $request, Pelanggan $pelanggan, $id) {
        $response = $pelanggan->validate();
        if($response === TRUE) {
            $pelanggan->find($id)->update($request->all());
            flash('Data pelanggan berhasil diperbarui')->success();
            return back();
        }
        return $response;
    }

    public function destroy($id, Pelanggan $pelanggan) {
        $pelanggan->destroy($id);
        flash('Data pelanggan berhasil dihapus')->success();
        return back();
    }

    public function export() {
        $pelanggan = Pelanggan::get();
        Excel::create('Data Pelanggan', function($excel) use($pelanggan) {
            $excel->sheet('SheetName', function($sheet) use($pelanggan) {
                $sheet->cells('A1:D1', function($cells) {
                    $cells->setFontWeight('bold');
                });

                $sheet->fromArray([
                    ['Nama Pelanggan', 'Alamat', 'No Telp', 'Email']
                ], null, 'A1', true, false);

                foreach ($pelanggan as $key => $value) {
                    $sheet->fromArray([
                        [$value->nama_pelanggan, $value->alamat, $value->no_telp, $value->email]
                    ], null, 'A1', false, false); 
                }
            });
        })->export('xlsx');
    }

    public function example() {
        Excel::create('Contoh Format Import Data Pelanggan', function($excel) {
            $excel->sheet('SheetName', function($sheet) {
                $sheet->cells('A1:D1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->fromArray([
                    ['Nama Pelanggan', 'Alamat', 'No Telp', 'Email']
                ], null, 'A1', true, false);
            });
        })->export('xlsx');
    }

    public function import(Request $request) {
        if($file = $request->file('excel_file')) {
            $v = Validator::make($request->all(), [
                'excel_file' => 'required|mimes:xlsx,xls'
            ]);
            if($v->fails()) {
                return back()->withErrors($v->errors());
            } else {
                $destinationPath = 'uploads';
                $file->move($destinationPath,$file->getClientOriginalName());
                $excel = Excel::load('uploads/'.$file->getClientOriginalName(), function($reader) {
                    $reader->skipRows(0);
                    foreach($reader->get() as $items) {
                        Pelanggan::create([
                            'nama_pelanggan' => $items->nama_pelanggan,
                            'alamat' => $items->alamat,
                            'no_telp' => $items->no_telp,
                            'email' => $items->email
                        ]);
                    }
                });
                flash('Data berhasil diupload.')->success();
                return back();
            }
        }
    }
}
