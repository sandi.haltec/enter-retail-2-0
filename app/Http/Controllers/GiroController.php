<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\Giro;
use App\Model\Jual;

class GiroController extends Controller
{

    public function index()
    {
        return view('giro.index');
    }

    public function getBelumCair() {
        return Datatables::of(Giro::query()->where('status', 0)->where('jatuh_tempo', date('Y-m-d')))->make(true);
    }

    public function getSudahCair() {
        return Datatables::of(Giro::query()->where('status', 1))->make(true);
    }

    public function getHangus() {
        return Datatables::of(Giro::query()->where('status', 2))->make(true);
    }

    public function create()
    {
        // $faktur = DB::table('jual')
        //             ->select('jual.*', 'jual_bayar.*', 'jual_lain.harga')
        //             ->where('no_faktur', '115100110058')
        //             ->leftJoin('jual_bayar', 'jual.id_jual', '=', 'jual_bayar.id_jual')
        //             ->leftJoin('jual_lain', 'jual.id_jual', '=', 'jual_lain.id_jual')
        //             ->first();
        // dd($faktur);

        // $faktur = Jual::with('jualBayar', 'jualLain')
        //             ->where('no_faktur', '115100110059')
        //             ->first();
        // foreach ($faktur->jualBayar as $key => $value) {
        //     $metode = $value->metode;
        //     $jumlah = $value->jumlah;
        // }
        // if ($faktur->jualLain) {
        //     foreach ($faktur->jualLain as $key => $value) {
        //         $harga = $value->harga;
        //         return $harga;
        //     }
        // }else{
        //     $harga = 0;
        //     dd($harga);
        // }
        // dd($faktur);

        return view('giro.create');
    }
    
    public function getFaktur($id) {
        $faktur = Jual::with('jualBayar', 'jualLain')
                    ->where('no_faktur', $id)
                    ->first();
        if($faktur) {
            foreach ($faktur->jualBayar as $key => $value) {
                $metode = $value->metode;
                $jumlah = $value->jumlah;
            }
            if (!$faktur->jualLain->isEmpty()) {
                foreach ($faktur->jualLain as $key => $value) {
                    $harga = $value->harga;
                }
            }else{
                $harga = 0;
            }

            return [
                'author' => $faktur->author,
                'jatuh_tempo' => date('d-m-Y', strtotime($faktur->jatuh_tempo)),
                'metode' => $metode,
                'ref' => $faktur->ref,
                'jual_lain' => $harga,
                'jumlah' => $jumlah,
                'diskon' => $faktur->diskon,
                'id_jual' => $faktur->id_jual
            ];
        } else {
            return [
                'err' => true,
                'item_total' => 0,
                'other_total' => 0,
                'total_debit' => 0,
                'total_credit' => 0,
                'due' => 0
            ];
        }
    }

    public function store(Request $request, Giro $giro)
    {
        $response = $giro->validate();
        if ($response === TRUE) {
            $giro->create($request->all());
            flash('Data giro berhasil disimpan.')->success();
            return redirect('giro');
        }
        return $response;
    }

    public function edit($id)
    {
        $giro = Giro::find($id);
        return view('giro.edit', compact('giro'));
    }

    public function update(Request $request, $id, Giro $giro)
    {
        $response = $giro->validate();  
        if ($response === TRUE) {
            $giro->find($id)->update($request->all());
            flash('Data giro berhasil diperbarui.')->success();
            return redirect('giro');
        }
        return $response;
    }

    public function destroy($id, Giro $giro)
    {
        $giro->destroy($id);
        flash('Data giro berhasil dihapus.')->success();
        return back();
    }

    public function cair($id, Giro $giro) {
        $giro->find($id)->update(['status'=>1, 'tanggal_cair'=>date('Y-m-d')]);
        flash('Data giro berhasil dicairkan.');
        return back();
    }

    public function hangus($id, Giro $giro) {
        $giro->find($id)->update(['status'=>2, 'tanggal_hangus'=>date('Y-m-d')]);
        flash('Data giro berhasil dihanguskan.');
        return back();
    }
}
