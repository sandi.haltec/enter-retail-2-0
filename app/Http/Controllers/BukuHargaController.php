<?php

namespace App\Http\Controllers;

use Excel;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\BukuHarga;

class BukuHargaController extends Controller
{

    public function index()
    {
        return view('buku_harga.index');
    }   

    public function get() {
        return Datatables::of(BukuHarga::query())->make(true);
    }

    public function create()
    {
        return view('buku_harga.create');
    }

    public function store(Request $request, BukuHarga $buku_harga)
    {
        $response = $buku_harga->validate();
        if ($response === TRUE) {
            $buku_harga->create($request->all());
            flash('Data buku harga berhasil disimpan.')->success();
            return back();
        }
        return $response;
    }

    public function edit($id)
    {
        $buku_harga = BukuHarga::find($id);
        return view('buku_harga.edit', compact('buku_harga'));
    }

    public function update(Request $request, $id, BukuHarga $buku_harga)
    {
        $response = $buku_harga->validate();
        if ($response === TRUE) {
            $buku_harga->find($id)->update($request->all());
            flash('Data buku harga berhasil diperbarui.')->success();
            return back();
        }
        return $response;
    }

    public function destroy($id, BukuHarga $buku_harga)
    {
        $buku_harga->destroy($id);
        flash('Data buku harga berhasil dihapus.');
        return back();
    }

    public function export() {
        $buku_harga =  BukuHarga::with('barang', 'pelanggan')->get();
        Excel::create('Data Buku Harga', function($excel) use($buku_harga) {
            $excel->sheet('SheetName', function($sheet) use($buku_harga){
                $sheet->cells('A1:E1', function($cells) {
                    $cells->setFontWeight('bold');
                });

                $sheet->fromArray([
                    ['ID Barang', 'Nama Barang', 'ID Pelanggan', 'Nama Pelanggan', 'Harga']
                ], null, 'A1', true, false);

                foreach ($buku_harga as $key => $value) {
                    $sheet->fromArray([
                        [$value->id_barang, $value->barang->nama_barang, $value->id_pelanggan, $value->pelanggan->nama_pelanggan, $value->harga]
                    ], null, 'A1', false, false);
                }
            });
        })->export('xlsx');
    }

    public function import(Request $request) {
        if ($file = $request->file('excel_file')) {
            $v = Validator::make($request->all(), [
                'excel_file' => 'required|mimes:xlsx,xls'
            ]);
            if($v->fails()) {
                return back()->withErrors($v->errors());
            } else {
                $destinationPath = 'uploads';
                $file->move($destinationPath, $file->getClientOriginalName());
                Excel::load('uploads/'.$file->getClientOriginalName(), function($reader) {
                    $reader->skipRows(0);
                    foreach ($reader->get() as $items) {
                        BukuHarga::create([
                            'id_barang' => $items->id_barang,
                            'id_pelanggan' => $items->id_pelanggan,
                            'harga' => $items->harga
                        ]);
                    }
                });
                return back();
            }
        }
    }

    public function example() {
        Excel::create('Contoh Import Data Buku Harga', function($excel) {
            $excel->sheet('SheetName', function($sheet) {
                $sheet->cells('A1:E1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->fromArray([
                    ['ID Barang', 'ID Pelanggan', 'Harga']
                ], null, 'A1', true, false);
            });
        })->export('xlsx');
    }
}
