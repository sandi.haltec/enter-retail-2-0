<?php

namespace App\Http\Controllers;

use App\Model\Satuan;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SatuanController extends Controller
{
    public function index() {
        return view('satuan.index');
    }

    public function get() {
        return Datatables::of(Satuan::query())->make(true);
    }

    public function create() {
        return view('satuan.create');
    }

    public function store(Request $request, Satuan $satuan) {
        $response = $satuan->validate(); 
        if($response === TRUE) { 
            $satuan->create($request->only('nama_satuan'));
            flash('Data satuan berhasil disimpan.')->success();
            return back(); 
        } 
        return $response; 
    }

    public function edit(Satuan $satuan, $id) {
        $satuan = $satuan->find($id);
        return view('satuan.edit', compact('satuan'));
    }

    public function update(Request $request, Satuan $satuan, $id) {
        $response = $satuan->validate(); 
        if($response === TRUE) { 
            $satuan->find($id)->update($request->only('nama_satuan'));
            flash('Data satuan berhasil diperbarui.')->success();
            return back(); 
        }
        return $response; 
    }

    public function destroy(Satuan $satuan, $id) {
        $satuan->destroy($id);
        flash('Data berhasil dihapus')->success();
        return back();
    }
}
