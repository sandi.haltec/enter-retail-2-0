<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\Jual;

class JualController extends Controller
{

    public function index()
    {
        return view('jual.index');
    }

    public function get(Request $request)
    {
        $query = Jual::with('pelanggan', 'jualBarang');
        if($startDate = $request->get('start_date')) {
            $query->where('tanggal', '>=', $startDate);
        }

        if($endDate = $request->get('end_date')) {
            $query->where('tanggal','<=', $endDate);
        }

        if($status = $request->get('status')) {
            $query->where('status', $status);
        }
        $query = $query->get();
        return Datatables::of($query)->make(true);
    }

    public function create()
    {
        return view('jual.create');
    }

    public function store(Request $request, Jual $jual)
    {
        $response = $jual->validate();
        if ($response === TRUE) {
            $jual->create($request->all());
            flash('Data penjualan berhasil disimpan.')->success();
            return redirect('penjualan');
        }
        return $response;
    }

    public function detail($id) {
        $jual = Jual::with('jualBarang.barang', 'jualBayar', 'jualLain')->first();
        return view('jual.detail', compact('jual'));
    }

    public function edit($id)
    {
        $jual = Jual::find($id);
        return view('jual.edit', compact('jual'));
    }

    public function update(Request $request, $id, Jual $jual)
    {
        $response = $jual->validate();
        if ($response === TRUE) {
            $jual->find($id)->update($request->all());
            flash('Data penjualan berhasil diperbarui')->success();
            return redirect('penjualan');
        }
        return $response;
    }

    public function destroy($id, Jual $jual)
    {
        $jual->destroy($id);
        flash('Data penjualan berhasil dihapus.')->success();
        return back();
    }
}
