<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use Validator;
use App\Model\Barang;
use App\Model\Satuan;
use App\Model\Kategori;
use App\Model\KategoriBarang;
use App\Model\SatuanHarga;
use App\Model\BarangStok;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class BarangController extends Controller
{
    public function index() {
        return view('barang.index');
    }

    public function get() {
        return Datatables::of(Barang::query())->make(true);
    }

    public function create() {
        $kategori = Kategori::pluck('nama_kategori', 'id_kategori');
        $satuan = Satuan::pluck('nama_satuan', 'id_satuan');
        return view('barang.create', compact('kategori', 'satuan'));
    }

    public function store(Request $request, Barang $barang) {
        $response = $barang->validate(); 
        if($response === TRUE) { 
            $idBarang = $barang->create($request->except('id_kategori'))->id_barang;
            foreach ($request->only('id_kategori')['id_kategori'] as $value) {
                KategoriBarang::create([
                    'id_kategori' => $value,
                    'id_barang' => $idBarang
                ]);
            }
            $post = $request->all();
            SatuanHarga::create([
                    'id_satuan_jual' => $post['id_satuan_jual'],
                    'id_satuan_beli' => $post['id_satuan_beli'],
                    'kode_barang' => $post['kode_barang'],
                    'harga_beli' => $post['harga_beli'],
                    'harga_jual' => $post['harga_jual'],
                    'pengali' => $post['pengali'],
                    'laba' => $post['laba']
            ]);
            BarangStok::create([
                    'kode_barang' => $post['kode_barang'],
                    'id_satuan_jual' => $post['id_satuan_jual'],
                    'stok_masuk' => $post['stok_masuk'],
                    'stok_keluar' => $post['stok_keluar'],
                    'stok_saat_ini' => $post['stok_saat_ini'],
                    'stok_minimal_keluar' => $post['stok_minimal_keluar'],
                    'stok_minimal' => $post['stok_minimal']
            ]);
            flash('Data barang berhasil disimpan.')->success();
            return back(); 
        } else { 
            return $response; 
        }   

    }

    public function edit($id) {
        $barang = Barang::find($id);
        $kategori = Kategori::pluck('nama_kategori', 'id_kategori');
        $kategoriBarang = KategoriBarang::where('id_barang', $id)->get();
        $satuan = Satuan::pluck('nama_satuan', 'id_satuan');
        $kategoriSelected = [];
        foreach ($kategoriBarang as $key => $value) {
            $kategoriSelected[$key] = $value->id_kategori;
        }
        $satuanHarga = SatuanHarga::where('kode_barang', $barang->kode_barang)->first();
        $barangStok = BarangStok::where('kode_barang', $barang->kode_barang)->first();
        return view('barang.edit', compact('barang', 'kategori', 'kategoriSelected', 'satuan', 'satuanHarga', 'barangStok'));
    }

    public function update(Request $request, Barang $barang, $id) {
        $response = $barang->validate();
        if($response === TRUE) {
            $barang->find($id)->update($request->except('id_kategori'));
            KategoriBarang::where('id_barang', $id)->delete();
            foreach ($request->only('id_kategori')['id_kategori'] as $value) {
                KategoriBarang::create([
                    'id_kategori' => $value,
                    'id_barang' => $id
                ]);
            }
            $post = $request->all();
            SatuanHarga::where('kode_barang', $post['kode_barang'])->update([
                'id_satuan_jual' => $post['id_satuan_jual'],
                'id_satuan_beli' => $post['id_satuan_beli'],
                'kode_barang' => $post['kode_barang'],
                'harga_beli' => $post['harga_beli'],
                'harga_jual' => $post['harga_jual'],
                'pengali' => $post['pengali'],
                'laba' => $post['laba']
            ]);
            BarangStok::where('kode_barang', $post['kode_barang'])->update([
                'id_satuan_jual' => $post['id_satuan_jual'],
                'stok_masuk' => $post['stok_masuk'],
                'stok_keluar' => $post['stok_keluar'],
                'stok_saat_ini' => $post['stok_saat_ini'],
                'stok_minimal_keluar' => $post['stok_minimal_keluar'],
                'stok_minimal' => $post['stok_minimal'],    
            ]);
            flash('Data barang berhasil diperbarui.')->success();
            return back(); 
        } else {
            return back()->withInput()->withErrors($response);
        }
    }

    public function destroy($id, Barang $barang) {
        $findBarang = $barang->find($id);
        $findBarang->delete($id);
        KategoriBarang::where('id_barang', $id)->delete();
        SatuanHarga::where('kode_barang', $findBarang->kode_barang)->delete();
        BarangStok::where('kode_barang', $findBarang->kode_barang)->delete();
        flash('Data barang berhasil dihapus.')->success();
        return back();
    }

    public function example() {
        Excel::create('Contoh Format Import Data Barang', function($excel) {
            $excel->sheet('SheetName', function($sheet) {
                $sheet->cells('A1:G1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->fromArray([
                    ['Kode Barang', 'Nama Barang', 'Barcode']
                ], null, 'A1', true, false);
            });
        })->export('xlsx');
    }

    public function export() {
        $barang = Barang::get();
        Excel::create('Data Barang', function($excel) use($barang) {
            $excel->sheet('SheetName', function($sheet) use($barang) {
                $sheet->cells('A1:H1', function($cells) {
                    $cells->setFontWeight('bold');
                });

                $sheet->fromArray([
                    ['Kode Barang', 'Nama Barang', 'Barcode']
                ], null, 'A1', true, false);

                foreach ($barang as $key => $value) {
                    $sheet->fromArray([
                        [$value->kode_barang, $value->nama_barang, $value->barcode]
                    ], null, 'A1', false, false); 
                }
            });
        })->export('xlsx');
    }

    public function import(Request $request) {
        if($file = $request->file('excel_file')) {
            $v = Validator::make($request->all(), [
                'excel_file' => 'required|mimes:xlsx,xls'
            ]);
            if($v->fails()) {
                return back()->withErrors($v->errors());
            } else {
                $destinationPath = 'uploads';
                $file->move($destinationPath,$file->getClientOriginalName());
                Excel::load('uploads/'.$file->getClientOriginalName(), function($reader) {
                    $reader->skipRows(0);
                    foreach($reader->get() as $items) {
                        Barang::create([
                            'kode_barang' => $items->kode_barang,
                            'nama_barang' => $items->nama_barang,
                            'barcode' => $items->barcode
                        ]);
                    }
                });
                return back();
            }
        }
    }

    public function find(Request $request) {
        $post = $request->all();
        $barang = Barang::select('barang.id_barang', 'barang.nama_barang', 'barang.ppn', 'satuan_harga.harga_beli', 'satuan.nama_satuan as satuan_beli', 'satuan_harga.id_satuan_beli')
                    ->leftJoin('satuan_harga', 'barang.kode_barang', '=', 'satuan_harga.kode_barang')
                    ->leftJoin('satuan', 'satuan.id_satuan', '=', 'satuan_harga.id_satuan_beli')
                    ->where('id_barang', $post['id'])
                    ->first();
        return json_encode($barang);
    }

    public function search(Request $request) {
        $response = [];
        $post = $request->get('term');
        $barang = Barang::where('nama_barang', 'like', '%'.$post.'%')
                        ->orWhere('kode_barang', 'like', '%'.$post.'%')
                        ->orWhere('kode_barang', $post)
                        ->orWhere('barcode', 'like', '%'.$post.'%')
                        ->orWhere('barcode', $post)
                        ->get();
        foreach ($barang as $key => $value) {
            $response[] = [
                'label' => $value->barcode . ' - ' . $value->nama_barang,
                'value' => $value->nama_barang,
                'id' => $value->id_barang
            ];
        }
        return json_encode($response);
    }

}
