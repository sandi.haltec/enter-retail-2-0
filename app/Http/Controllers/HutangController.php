<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Model\Hutang;
use App\Model\Beli;
class HutangController extends Controller
{

    public function index()
    {
        return view('hutang.index');
    }

    public function get() {
        $query = Beli::where('status', 0)
                    ->get();
        return Datatables::of($query)->make(true);
    }


    public function create()
    {
        return view('hutang.create');
    }

    public function getFaktur($id) {
        $faktur = Beli::with('beliBarang', 'beliBayar' ,'beliLain')
                    ->where('no_faktur', $id)
                    ->where('status', '0')
                    ->first();
        if(!is_null($faktur)) {
            foreach ($faktur->beliBayar as $key => $value) {
                $metode = $value->metode;
                $ref = $value->ref;
                $jumlah = $value->jumlah;
            }
            if (!$faktur->beliLain->isEmpty()) {
                foreach ($faktur->beliLain as $key => $value) {
                    $harga = $value->harga;
                }
            }else{
                $harga = 0;
            }

            $grandTotal = 0;
            foreach ($faktur->beliBarang()->get() as $beliBarang) {
                $grandTotal += $beliBarang->total;
            }

            $biayaLain = 0;
            foreach ($faktur->beliLain()->get() as $beliLain) {
                $biayaLain += $beliLain->harga;
            }

            return [
                'err' => false,
                'beliBayar' => $faktur->beliBayar,
                'biayaLain' => $biayaLain,
                'grandTotal' => $grandTotal,
                'diskon' => $faktur->diskon,
                'id_beli' => $faktur->id_beli
            ];
        } else {
            return [
                'err' => true,
                'item_total' => 0,
                'other_total' => 0,
                'total_debit' => 0,
                'total_credit' => 0,
                'due' => 0
            ];
        }
    }

    public function detail($id) {
        $detail = Beli::with('supplier')
                    ->where('no_faktur', $id)
                    ->first();
        dd($detail);
        return view('hutang.detail', compact('detail')); 
    }

    public function store(Request $request, Hutang $hutang)
    {
        $total_debit = $request->all()['total_debit']+$request->all()['jumlah'];
        if ($total_debit >= $request->all()['total_credit'] || $request->all()['status'] == 1) {
            Beli::find($request->all()['id_beli'])->update(['status' => 1]);
        }else{
            return "belum lunas";
        }
        $response = $hutang->validate($request->all());
        if ($response === TRUE) {
            $hutang->create($request->all());
            flash('Data hutang berhasil disimpan')->success();
            return redirect('hutang');
        }else{
            return back()->withInput()->withErrors($response);
        }
    }

    public function edit($id)
    {
        $hutang = DB::table('pembayaran')
                        ->select('pembayaran.*','beli.no_faktur', 'pembayaran.status')
                        ->join('beli', 'pembayaran.id_beli', '=', 'beli.id_beli')
                        ->first();
        return view('hutang.edit', compact('hutang'));
    }

    public function update(Request $request, $id, Hutang $hutang)
    {
        $response = $hutang->validate($request->all());
        if ($response === TRUE) {
            $hutang->find($id)->update($request->all());
            flash('Data hutang berhasil disimpan.')->success();
            return back();
        }else{
            return back()->withInput()->withErrors($response);
        }
    }

    public function destroy($id, Hutang $hutang)
    {
        $hutang->destroy($id);
        flash('Data hutang berhasil dihapus.');
        return back();
    }
}
