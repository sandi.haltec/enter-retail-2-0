<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class BeliRetur extends BaseModel
{
    protected $primaryKey = 'id_beli_retur';

    protected $table = 'beli_retur';

    public $timestamps = false;

    protected $fillable = [
    	'id_beli',
    	'id_barang',
    	'tanggal',
    	'kuantitas',
    	'diskon',
    	'harga'
    ];

    public $rules = [
    	'id_beli' => 'required',
    	'id_barang' => 'required',
    	'tanggal' => 'required',
    	'kuantitas' => 'required|numeric',
    	'diskon' => 'required|numeric',
    	'harga' => 'required|numeric'
    ];
}
