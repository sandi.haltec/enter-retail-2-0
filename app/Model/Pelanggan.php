<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Pelanggan extends BaseModel
{
    protected $table = 'pelanggan';

    protected $primaryKey = 'id_pelanggan';

    public $timestamps = false;

    protected $fillable = [
        'nama_pelanggan',
        'alamat',
        'no_telp',
        'email',
        'pembelian',
        'waktu_transaksi'
    ];

    public $rules = [
        'nama_pelanggan' => 'required',
        'alamat' => 'required',
        'no_telp' => 'required|numeric',
        'email' => 'required|email'
    ];

    public function buku_harga() {
        return $this->hasMany('App\Model\BukuHarga', 'id_pelanggan');
    }

    public function jual() {
        return $this->hasMany(Jual::class, 'id_pelanggan');
    }
}
