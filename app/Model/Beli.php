<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Beli extends BaseModel
{
    protected $table = 'beli';

    protected $primaryKey = 'id_beli';

    protected $fillable = [
        'no_faktur',
        'tanggal',
        'jatuh_tempo',
        'id_supplier',
        'total',
        'diskon',
        'ppn',
        'status',
        'tangguhkan',
        'author'
    ];

    public $rules = [
        'no_faktur' => 'required',
        'tanggal' => 'required',
        'id_supplier' => 'required',
        'jatuh_tempo' => 'required'
    ];

    public function validation() {
        $v = Validator::make(request()->all(), $this->rules);
        if($v->fails()) {
            return $v->errors()->all();
        }
        return true;
    }

    public function setTanggalAttribute($value) {
        $this->attributes['tanggal'] = CarbonDate($value, 'Y-m-d');
    }

    public function setJatuhTempoAttribute($value) {
        $this->attributes['jatuh_tempo'] = CarbonDate($value, 'Y-m-d');
    }

    public function getTanggalAttribute($value) {
        return CarbonDate($value, 'd-m-Y');
    }

    public function beliBarang() {
        return $this->hasMany(BeliBarang::class, 'id_beli');
    }

    public function beliBayar() {
        return $this->hasMany(BeliBayar::class, 'id_beli');
    }

    public function beliLain() {
        return $this->hasMany(BeliLain::class, 'id_beli');
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'id_supplier');
    }

    public function satuan() {
        return $this->hasOne(Satuan::class, 'id_satuan');
    }
}
