<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends BaseModel
{
	protected $primaryKey = 'id_supplier';

	protected $table = 'supplier';

    public $timestamps = false;

    protected $fillable = [
    	'nama_supplier',
    	'alamat',
    	'no_telp',
    	'email',
    	'penjualan',
    	'waktu_penjualan'
    ];

    public $rules = [
    	'nama_supplier' => 'required',
    	'alamat' => 'required',
    	'no_telp' => 'required|numeric',
    	'email' => 'required|email'
    ];

    public function beli() {
        return $this->hasOne(Beli::class, 'id_supplier');
    }

}
