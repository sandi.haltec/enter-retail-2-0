<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JualBayar extends Model
{
    protected $primaryKey = 'id_jual_bayar';

    protected $table = 'jual_bayar';

    public $timestamps = false;

    protected $fillable = [
    	'id_jual',
    	'tanggal',
    	'metode',
    	'ref',
    	'status',
    	'jumlah',
    	'author'
    ];

    public function jual() {
        return $this->belongsTo(Jual::class, 'id_jual');
    }
}
