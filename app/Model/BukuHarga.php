<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BukuHarga extends BaseModel
{
    protected $primaryKey = 'id_buku_harga';

    protected $table = 'buku_harga';

    public $timestamps = false;

    protected $fillable = [
    	'id_barang',
    	'id_pelanggan',
    	'harga'
    ];

    public $rules = [
    	'id_barang' => 'required',
    	'id_pelanggan' => 'required',
    	'harga' => 'required|numeric'
    ];

    public function barang() {
        return $this->belongsTo('App\Model\Barang', 'id_barang');
    }

    public function pelanggan() {
        return $this->belongsTo('App\Model\pelanggan', 'id_pelanggan');
    }

}
