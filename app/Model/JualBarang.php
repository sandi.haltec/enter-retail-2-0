<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JualBarang extends Model
{
    protected $primaryKey = 'id_jual_barang';

    protected $table = 'jual_barang';

    public $timestamps = false;

    protected $fillable = [
    	'id_jual',
    	'id_barang',
    	'harga',
    	'kuantitas',
    	'diskon',
    	'total'
    ];

    public function jual() {
        return $this->belongsTo(Jual::class, 'id_jual');
    }

    public function barang() {
        return $this->belongsTo(Barang::class, 'id_barang');
    }

}
