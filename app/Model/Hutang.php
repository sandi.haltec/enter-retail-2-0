<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Hutang extends BaseModel
{
    protected $primaryKey = 'id_pembayaran';

    protected $table = 'pembayaran';

    public $timestamps = false;

    protected $fillable = [
    	'id_beli',
    	'tanggal',
    	'metode',
    	'ref',
    	'jumlah',
    	'status',
    	'author'
    ];

    public $rules = [
    	'id_beli'=>'required',
    	'tanggal'=>'required',
    	'metode'=>'required',
    	'jumlah'=>'required',
    ];

    public function beli() {
    	return $this->belongsTo('App\Model\Beli', 'id_beli');
    }
}
