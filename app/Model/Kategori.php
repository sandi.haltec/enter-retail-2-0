<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Kategori extends BaseModel
{
    protected $primaryKey = 'id_kategori';
    
    protected $table = 'kategori';

    public $timestamps = false;

    protected $fillable = [
        'nama_kategori',
        'created_at',
        'updated_at'
    ];

    public $rules = [
        'nama_kategori' => 'required',
    ];

}
    