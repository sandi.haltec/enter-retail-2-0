<?php

namespace App\Model;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function validate() {
        $v = Validator::make(request()->all(), $this->rules);
        if($v->fails()) {
            session()->flash('errors', $v->errors());
            return back()->withInput();
        }
        return true;
    }
    
}
