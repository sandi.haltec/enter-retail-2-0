<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BarangStok extends BaseModel
{
    protected $primaryKey = 'id_harga_barang';

    protected $table = 'barang_stok';

    public $timestamps = false;

    protected $fillable = [
    	'kode_barang',
    	'id_satuan_jual',
    	'stok_masuk',
    	'stok_keluar',
    	'stok_saat_ini',
    	'stok_minimal_keluar',
    	'stok_minimal'
    ];
}
