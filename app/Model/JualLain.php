<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JualLain extends Model
{
    protected $primaryKey = 'id_jual_lain';

    protected $table = 'jual_lain';

    public $timestamps = false;

    protected $fillable = [
    	'id_jual',
    	'harga',
    	'deskripsi'
    ];

    public function jual() {
        return $this->belongsTo('App\Model\Jual', 'id_jual');
    }
}
