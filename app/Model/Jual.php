<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Jual extends BaseModel
{
    protected $primaryKey = 'id_jual';

    protected $table = 'jual';

    protected $fillable = [
    	'id_pelanggan',
    	'id_bank',
    	'ref',
    	'tanggal',
    	'jatuh_tempo',
    	'diskon',
    	'tipe',
    	'status',
    	'tangguhkan',
    	'author'
    ];

    public $rules = [
    	'id_pelanggan' => 'required',
    	'id_bank' => 'required',
    	'ref' => 'required',
    	'tanggal' => 'required',
    	'jatuh_tempo' => 'required',
    	'diskon' => 'required',
    	'tipe' => 'required',
    	'status' => '',
    	'tangguhkan' => '',
    	'author' => ''
    ];

    public function jualBarang() {
        return $this->hasMany(JualBarang::class, 'id_jual');
    }

    public function jualBayar() {
        return $this->hasMany(JualBayar::class, 'id_jual');
    }

    public function jualLain() {
        return $this->hasMany(JualLain::class, 'id_jual');
    }

    public function getTanggalAttribute($value) {
        return CarbonDate($value);
    }

    public function getTotalAttribute($value) {
        return numberToPrice($value);
    }

    public function getStatusAttribute($value) {
        if($value == 0) {
            return 'Belum Lunas';
        } else if($value == 1) {
            return 'Lunas';
        }
    }

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class, 'id_pelanggan');
    }
}
