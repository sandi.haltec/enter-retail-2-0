<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Barang extends BaseModel
{
    protected $primaryKey = 'id_barang';
    
    protected $table = 'barang';

    public $timestamps = false;

    protected $fillable = [
        'kode_barang',
        'nama_barang',
        'barcode',
        'ppn',
        'keterangan'
    ];

    public $rules = [
        'kode_barang' => 'required',
        'nama_barang' => 'required',
        'barcode' => 'required'
    ];

    public function bukuHarga() {
        return $this->hasMany(BukuHarga::class, 'id_barang');
    }

    public function jualBarang() {
        return $this->hasMany(JualBarang::class, 'id_barang');
    }

    public function satuanHarga() {
        return $this->hasMany(SatuanHarga::class, 'kode_barang');
    }
}
    