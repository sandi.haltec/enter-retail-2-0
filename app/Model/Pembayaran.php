<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $primaryKey = 'id_pembayaran';

    protected $table = 'pembayaran';

    protected $fillable = [
        'id_beli',
        'tanggal',
        'metode',
        'ref',
        'jumlah',
        'status',
        'author'
    ];

    public $timestamps = false;

    public function setTanggalAttribute($value) {
        $this->attributes['tanggal'] = CarbonDate($value, 'Y-m-d');
    }

    public function getTanggalAttribute($value) {
        return CarbonDate($value, 'd-m-Y');
    }

    public function getMetodeAttribute($value) {
        if($value == '1') {
            return 'Cash';
        } elseif($value == '2') {
            return 'Giro';
        } elseif ($value == '3') {
            return 'Kredit';
        }
    }

    public function beli() {
        return $this->belongsTo(Beli::class, 'id_beli');
    }
}
    