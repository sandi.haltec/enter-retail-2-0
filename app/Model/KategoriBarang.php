<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriBarang extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'kategori_barang';

    public $timestamps = false;

    protected $fillable = [
    	'id_kategori',
    	'id_barang'
    ];
}
