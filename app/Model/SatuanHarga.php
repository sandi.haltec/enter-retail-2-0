<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SatuanHarga extends BaseModel
{
	protected $primaryKey = 'id';

	protected $table = 'satuan_harga';

	public $timestamps = false;

	protected $fillable = [
		'id_satuan_jual',
		'id_satuan_beli',
		'kode_barang',
		'harga_beli',
		'harga_jual',
		'pengali',
        'laba'
	];

    public function setHargaBeliAttribute($value) {
        $this->attributes['harga_beli'] = priceToNumber($value);
    }

    public function setHargaJualAttribute($value) {
        $this->attributes['harga_jual'] = priceToNumber($value);
    }

    public function setLabaAttribute($value) {
        $this->attributes['laba'] = priceToNumber($value);
    }

    public function barang() {
        return $this->belongsTo(Barang::class, 'kode_barang');
    }
}
