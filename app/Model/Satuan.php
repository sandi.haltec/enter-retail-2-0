<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Satuan extends BaseModel
{
    protected $primaryKey = 'id_satuan';
    
    protected $table = 'satuan';

    public $timestamps = false;

    protected $fillable = [
        'nama_satuan'
    ];

    public $rules = [
        'nama_satuan' => 'required'
    ];

    public function beli() {
        return $this->hasOne(Beli::class, 'id_satuan_beli');
    }

}
