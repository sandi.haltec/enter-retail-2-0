<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JualRetur extends BaseModel
{
    protected $primaryKey = 'id_jual_retur';

    protected $table = 'jual_retur';

    public $timestamps = false;

    protected $fillable = [
    	'id_jual',
    	'id_barang',
    	'tanggal',
    	'kuantitas',
    	'diskon',
    	'harga',
    	'total',
    	'alasan',
    	'status',
    	'author'
    ];

    public $rules = [
    	'id_jual' => 'required',
    	'id_barang' => 'required',
    	'tanggal' => 'required',
    	'kuantitas' => 'required',
    	'diskon' => 'required',
    	'harga' => 'required',
    	'total' => 'required',
    	'alasan' => '',
    	'status' => '',
    	'author' => ''
    ];

    public function setDiskonAttribute($value) {
        $this->attributes['diskon'] = priceToNumber($value);
    }

    public function setHargaAttribute($value) {
        $this->attributes['harga'] = priceToNumber($value);
    }

    public function setTotalAttribute($value) {
        $this->attributes['total'] = priceToNumber($value);
    }


}
