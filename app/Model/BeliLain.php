<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BeliLain extends Model
{
    protected $primaryKey = 'id_beli_lain';

    protected $table = 'beli_lain';

    public $timestamps = false;

    protected $fillable = [
    	'id_beli',
    	'harga',
    	'deskripsi'
    ];

    public function beli() {
        return $this->belongsTo(Beli::class, 'id_beli');
    }
}
