<?php

namespace App\Model;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Giro extends BaseModel
{
    protected $primaryKey = 'id_giro';

    protected $table = 'giro';

    public $timestamps = false;

    protected $fillable = [
    	'id_penjualan',
    	'no_ref',
    	'no_faktur',
    	'tanggal_buat',
    	'tanggal_cair',
    	'tanggal_hangus',
    	'jatuh_tempo',
    	'total',
    	'jenis',
    	'status',
    	'author'
    ];

    public $rules = [
        'id_penjualan' => 'required',
        'no_faktur' => 'required',
        'jatuh_tempo' => 'required',
        'total' => 'required',
        'status' => '',
        'author' => ''
    ];
}
