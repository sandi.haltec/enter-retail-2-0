<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BeliBarang extends BaseModel
{
    protected $primaryKey = 'id_beli_barang';

    protected $table = 'beli_barang';

    public $timestamps = false;

    protected $fillable = [
    	'id_beli',
        'id_barang',
    	'id_satuan_beli',
    	'harga',
    	'jumlah',
    	'diskon',
    	'total',
        'ppn',
        'prosen_ppn',
    	'expired'
    ];

    public function setExpiredAttribute($value) {
        $this->attributes['expired'] = CarbonDate($value, 'Y-m-d');
    }

    public function barang() {
        return $this->belongsTo(Barang::class, 'id_barang');
    }

    public function beli() {
        return $this->belongsTo(Beli::class, 'id_beli');
    }
}
