<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('barang.index');
});

Route::prefix('barang')->group(function() {
    Route::get('/', 'BarangController@index')->name('barang.index');
    Route::get('create', 'BarangController@create')->name('barang.create');
    Route::post('store', 'BarangController@store')->name('barang.store');
    Route::get('edit/{id}', 'BarangController@edit')->name('barang.edit');
    Route::post('update/{id}', 'BarangController@update')->name('barang.update');
    Route::get('destroy/{id}', 'BarangController@destroy')->name('barang.destroy');
    Route::get('export', 'BarangController@export')->name('barang.export');
    Route::get('example', 'BarangController@example')->name('barang.example');
    Route::post('import', 'BarangController@import')->name('barang.import');
});

Route::prefix('kategori')->group(function() {
	Route::get('/', 'KategoriController@index')->name('kategori.index');
	Route::get('create', 'KategoriController@create')->name('kategori.create');
	Route::post('store', 'KategoriController@store')->name('kategori.store');
    Route::get('edit/{id}', 'KategoriController@edit')->name('kategori.edit');
    Route::post('update/{id}', 'KategoriController@update')->name('kategori.update');
    Route::get('destroy/{id}', 'KategoriController@destroy')->name('kategori.destroy');
});

Route::prefix('satuan')->group(function() {
    Route::get('/', 'SatuanController@index')->name('satuan.index');
    Route::get('create', 'SatuanController@create')->name('satuan.create');
    Route::post('store', 'SatuanController@store')->name('satuan.store');
    Route::get('edit/{id}', 'SatuanController@edit')->name('satuan.edit');
    Route::post('update/{id}', 'SatuanController@update')->name('satuan.update');
    Route::get('destroy/{id}', 'SatuanController@destroy')->name('satuan.destroy');
});

Route::prefix('supplier')->group(function() {
	Route::get('/', 'SupplierController@index')->name('supplier.index');
	Route::get('create', 'SupplierController@create')->name('supplier.create');
	Route::post('store', 'SupplierController@store')->name('supplier.store');
    Route::get('edit/{id}', 'SupplierController@edit')->name('supplier.edit');
    Route::post('update/{id}', 'SupplierController@update')->name('supplier.update');
    Route::get('destroy/{id}', 'SupplierController@destroy')->name('supplier.destroy');
    Route::get('export', 'SupplierController@export')->name('supplier.export');
    Route::get('example', 'SupplierController@example')->name('supplier.example');
    Route::post('import', 'SupplierController@import')->name('supplier.import');
});

Route::prefix('buku_harga')->group(function() {
    Route::get('/', 'BukuHargaController@index')->name('buku_harga.index');
    Route::get('create', 'BukuHargaController@create')->name('buku_harga.create');
    Route::post('store', 'BukuHargaController@store')->name('buku_harga.store');
    Route::get('edit/{id}', 'BukuHargaController@edit')->name('buku_harga.edit');
    Route::post('update/{id}', 'BukuHargaController@update')->name('buku_harga.update');
    Route::get('destroy/{id}', 'BukuHargaController@destroy')->name('buku_harga.destroy');
    Route::get('export', 'BukuHargaController@export')->name('buku_harga.export');
    Route::get('example', 'BukuHargaController@example')->name('buku_harga.example');
    Route::post('import', 'BukuHargaController@import')->name('buku_harga.import');
});

Route::prefix('pelanggan')->group(function() {
    Route::get('/', 'PelangganController@index')->name('pelanggan.index');
    Route::get('create', 'PelangganController@create')->name('pelanggan.create');
    Route::post('store', 'PelangganController@store')->name('pelanggan.store');
    Route::get('edit/{id}', 'PelangganController@edit')->name('pelanggan.edit');
    Route::post('update/{id}', 'PelangganController@update')->name('pelanggan.update');
    Route::get('destroy/{id}', 'PelangganController@destroy')->name('pelanggan.destroy');
    Route::get('export', 'PelangganController@export')->name('pelanggan.export');
    Route::get('example', 'PelangganController@example')->name('pelanggan.example');
    Route::post('import', 'PelangganController@import')->name('pelanggan.import');
}); 

Route::prefix('pembelian')->group(function() {
    Route::get('/', 'BeliController@index')->name('beli.index');
    Route::get('create', 'BeliController@create')->name('beli.create');
    Route::post('store', 'BeliController@store')->name('beli.store');
    Route::get('detail/{id}', 'BeliController@detail')->name('beli.detail');
    Route::get('tangguhkan/{id}', 'BeliController@tangguhkan')->name('beli.tangguhkan');
    Route::get('destroy/{id}', 'BeliController@destroy')->name('beli.destroy');
    Route::get('edit/{id}', 'BeliController@edit')->name('beli.edit');
});

Route::prefix('penjualan')->group(function() {
    Route::get('/', 'JualController@index')->name('jual.index');
    Route::get('create', 'JualController@create')->name('jual.create');
    Route::post('store', 'JualController@store')->name('jual.store');
    Route::get('edit/{id}', 'JualController@edit')->name('jual.edit');
    Route::get('detail/{id}', 'JualController@detail')->name('jual.detail');
    Route::post('update/{id}', 'JualController@update')->name('jual.update');
    Route::get('destroy/{id}', 'JualController@destroy')->name('jual.destroy');
    // Route::get('loadData', 'JualController@loadData')->name('jual.loadData');
});

Route::prefix('hutang')->group(function() {
    Route::get('/', 'HutangController@index')->name('hutang.index');
    Route::get('create', 'HutangController@create')->name('hutang.create');
    Route::post('store', 'HutangController@store')->name('hutang.store');
    Route::get('edit/{id}', 'HutangController@edit')->name('hutang.edit');
    Route::post('update/{id}', 'HutangController@update')->name('hutang.update');
    Route::get('destroy/{id}', 'HutangController@destroy')->name('hutang.destroy');
    Route::get('getFaktur/{id}', 'HutangController@getFaktur')->name('hutang.getFaktur');
    Route::get('detail/{id}', 'HutangController@detail')->name('hutang.detail');
});

Route::prefix('beli_retur')->group(function() {
    Route::get('/', 'BeliReturController@index')->name('beli_retur.index');
    Route::get('create', 'BeliReturController@create')->name('beli_retur.create');
    Route::post('store', 'BeliReturController@store')->name('beli_retur.store');
    Route::get('edit/{id}', 'BeliReturController@edit')->name('beli_retur.edit');
    Route::post('update/{id}', 'BeliReturController@update')->name('beli_retur.update');
    Route::get('destroy/{id}', 'BeliReturController@destroy')->name('beli_retur.destroy');
    Route::get('getFaktur/{id}', 'BeliReturController@getFaktur')->name('beli_retur.getFaktur');
});

Route::prefix('jual_retur')->group(function() {
    Route::get('/', 'JualReturController@index')->name('jual_retur.index');
    Route::get('create', 'JualReturController@create')->name('jual_retur.create');
    Route::post('store', 'JualReturController@store')->name('jual_retur.store');
    Route::get('edit/{id}', 'JualReturController@edit')->name('jual_retur.edit');
    Route::post('update/{id}', 'JualReturController@update')->name('jual_retur.update');
    Route::get('destroy/{id}', 'JualReturController@destroy')->name('jual_retur.destroy');
    Route::get('getFaktur/{id}', 'JualReturController@getFaktur')->name('jual_retur.getFaktur');
});

Route::prefix('giro')->group(function() {
    Route::get('/', 'GiroController@index')->name('giro.index');
    Route::get('create', 'GiroController@create')->name('giro.create');
    Route::post('store', 'GiroController@store')->name('giro.store');
    Route::get('edit/{id}', 'GiroController@edit')->name('giro.edit');
    Route::post('update/{id}', 'GiroController@update')->name('giro.update');
    Route::get('destroy/{id}', 'GiroController@destroy')->name('giro.destroy');
    Route::get('getFaktur/{id}', 'GiroController@getFaktur')->name('giro.getFaktur');
    Route::get('cair/{id}', 'GiroController@cair')->name('giro.cair');
    Route::get('hangus/{id}', 'GiroController@hangus')->name('giro.hangus');
});