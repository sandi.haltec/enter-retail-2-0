<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('barang/get', 'BarangController@get');
Route::get('kategori/get', 'KategoriController@get');
Route::get('satuan/get', 'SatuanController@get');
Route::get('pelanggan/get', 'PelangganController@get');
Route::get('buku_harga/get', 'BukuHargaController@get');
Route::get('supplier/get', 'SupplierController@get');
Route::post('supplier/find', 'SupplierController@find');
Route::post('barang/find', 'BarangController@find');
Route::get('barang/search', 'BarangController@search');
Route::get('supplier/search', 'SupplierController@search');
Route::get('hutang/get', 'HutangController@get');
Route::get('jual/get', 'JualController@get');
Route::get('jual/loadData', 'JualController@loadData');
Route::get('jual_retur/get', 'JualReturController@get');
Route::get('beli_retur/get', 'BeliReturController@get');
Route::get('giro/getBelumCair', 'GiroController@getBelumCair');
Route::get('giro/getSudahCair', 'GiroController@getSudahCair');
Route::get('giro/getHangus', 'GiroController@getHangus');
Route::get('pembelian/get', 'BeliController@get');
Route::post('pembelian/store', 'BeliController@store');