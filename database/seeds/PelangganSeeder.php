<?php

use Illuminate\Database\Seeder;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelanggan')->truncate();
        DB::table('pelanggan')->insert([
            [
                'nama_pelanggan' => 'Arif',
                'alamat' => '-',
                'no_telp' => '081555888999',
                'email' => 'arif@gmail.com',
            ],
            [
                'nama_pelanggan' => 'Rifai',
                'alamat' => '-',
                'no_telp' => '081333444555',
                'email' => 'rifai@gmail.com',
            ],
            [
                'nama_pelanggan' => 'Pinki',
                'alamat' => '-',
                'no_telp' => '0822233399990',
                'email' => 'pinki@gmail.com',
            ],
        ]);
    }
}
