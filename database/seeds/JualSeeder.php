<?php

use Illuminate\Database\Seeder;

class JualSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jual')->truncate();
        DB::table('jual')->insert([
            [
                'no_faktur' => '123456',
                'tanggal' => CarbonDate('now', 'Y-m-d'),
                'jatuh_tempo' => CarbonDate('now', 'Y-m-d'),
                'id_pelanggan' => 1,
                'potongan' => 0,
                'ppn' => 0,
                'total' => 10000,
                'tipe' => 0,
                'status' => 0,
                'tangguhkan' => 0,
                'author' => 'admin'
            ],
            [
                'no_faktur' => '9876543',
                'tanggal' => CarbonDate('now', 'Y-m-d'),
                'jatuh_tempo' => CarbonDate('now', 'Y-m-d'),
                'id_pelanggan' => 2,
                'potongan' => 0,
                'ppn' => 0,
                'total' => 12000,
                'tipe' => 0,
                'status' => 0,
                'tangguhkan' => 0,
                'author' => 'admin'
            ]
        ]);
    }
}
