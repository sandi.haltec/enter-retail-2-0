<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang')->truncate();
        DB::table('barang')->insert([
            [
                'kode_barang' => '001001',
                'nama_barang' => 'Panadol',
                'barcode' => '001001001',
            ],
            [
                'kode_barang' => '001002',
                'nama_barang' => 'Paramex',
                'barcode' => '001002002',
            ],
            [
                'kode_barang' => '001003',
                'nama_barang' => 'Stop Cold',
                'barcode' => '001003003'
            ]
        ]);
        DB::table('barang_stok')->truncate();
        DB::table('barang_stok')->insert([
            [
                'kode_barang' => '001001',
                'id_satuan_jual' => '2',
                'stok_masuk' => 10,
                'stok_keluar' => 0,
                'stok_saat_ini' => 10,
                'stok_minimal_keluar' => 1,
                'stok_minimal' => 1
            ],
            [
                'kode_barang' => '001002',
                'id_satuan_jual' => '2',
                'stok_masuk' => 10,
                'stok_keluar' => 0,
                'stok_saat_ini' => 10,
                'stok_minimal_keluar' => 1,
                'stok_minimal' => 1
            ],
            [
                'kode_barang' => '001003',
                'id_satuan_jual' => '3',
                'stok_masuk' => 10,
                'stok_keluar' => 0,
                'stok_saat_ini' => 10,
                'stok_minimal_keluar' => 1,
                'stok_minimal' => 1
            ],
        ]);
        DB::table('satuan_harga')->truncate();
        DB::table('satuan_harga')->insert([
            [
                'id_satuan_jual' => '2',
                'id_satuan_beli' => '2',
                'kode_barang' => '001001',
                'harga_beli' => '12000',
                'harga_jual' => '14000',
                'pengali' => '1',
                'laba' => '1000'
            ],
            [
                'id_satuan_jual' => '2',
                'id_satuan_beli' => '2',
                'kode_barang' => '001002',
                'harga_beli' => '13000',
                'harga_jual' => '15000',
                'pengali' => '1',
                'laba' => '1000'
            ],
            [
                'id_satuan_jual' => '3',
                'id_satuan_beli' => '1',
                'kode_barang' => '001003',
                'harga_beli' => '11000',
                'harga_jual' => '17000',
                'pengali' => '10',
                'laba' => '1000'
            ]
        ]);
    }
}
