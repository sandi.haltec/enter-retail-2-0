<?php

use Illuminate\Database\Seeder;

class JualBayarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jual_bayar')->truncate();
        DB::table('jual_bayar')->insert([
            [
                'id_jual' => 1,
                'id_bank' => 1,
                'tanggal' => CarbonDate('now', 'Y-m-d'),
                'metode' => 1,
                'ref' => 0001231,
                'status' => 1,
                'jumlah' => 22000,
                'author' => 'admin'
            ]
        ]);
    }
}
