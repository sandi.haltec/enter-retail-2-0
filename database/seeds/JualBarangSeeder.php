<?php

use Illuminate\Database\Seeder;

class JualBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jual_barang')->truncate();
        DB::table('jual_barang')->insert([
            [
                'id_jual' => 1,
                'id_barang' => 1,
                'harga' => 1000,
                'kuantitas' => 10,
                'potongan' => 0,
                'ppn' => 0,
                'total' => 10000
            ],
            [
                'id_jual' => 1,
                'id_barang' => 2,
                'harga' => 6000,
                'kuantitas' => 2,
                'potongan' => 0,
                'ppn' => 0,
                'total' => 12000
            ]
        ]);
    }
}
