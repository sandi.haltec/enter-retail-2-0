<?php

use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplier')->truncate();
        DB::table('supplier')->insert([
            [
                'nama_supplier' => 'PT. Maju Farma',
                'alamat' => 'Kaliurang, Jogjakarta',
                'no_telp' => '0341 9980909',
                'email' => 'majufarma@pt.com'
            ],
            [
                'nama_supplier' => 'PT. Farma Putra',
                'alamat' => 'Malang, Jawa Timur',
                'no_telp' => '081231231248',
                'email' => 'farmaputra@pt.com'
            ],
            [
                'nama_supplier' => 'CV. Anugrah Sehat',
                'alamat' => 'Tulungagung, Jawa Timur',
                'no_telp' => '+6281998776532',
                'email' => 'anugrahsehat@cvku.com'
            ],
        ]);
    }
}
