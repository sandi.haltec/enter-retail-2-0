<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori')->truncate();
        DB::table('kategori')
          ->insert([
            [
                'id_kategori' => '1',
                'nama_kategori' => 'Celana',    
            ],
            [
                'id_kategori' => '2', 
                'nama_kategori' => 'Kaos'
            ],
            [
                'id_kategori' => '3',
                'nama_kategori' => 'Baju'
            ]
        ]);
    }
}
