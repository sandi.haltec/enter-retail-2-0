<?php

use Illuminate\Database\Seeder;

class SatuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('satuan')->truncate();

        DB::table('satuan')->insert([
            ['id_satuan' => '1', 'nama_satuan' => 'Sendok'],
            ['id_satuan' => '2', 'nama_satuan' => 'Tablet'],
            ['id_satuan' => '3', 'nama_satuan' => 'Botol'],
        ]);
    }
}
