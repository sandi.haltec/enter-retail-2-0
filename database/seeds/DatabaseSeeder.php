<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KategoriSeeder::class);
        $this->call(SatuanSeeder::class);
        $this->call(SupplierSeeder::class);
        $this->call(BarangSeeder::class);
        $this->call(PelangganSeeder::class);
        $this->call(JualSeeder::class);
        $this->call(JualBarangSeeder::class);
        $this->call(JualBayarSeeder::class);
    }
}
