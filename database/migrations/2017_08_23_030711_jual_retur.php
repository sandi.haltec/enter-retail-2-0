<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JualRetur extends Migration
{

    public function up()
    {
        Schema::create('jual_retur', function(Blueprint $table){
            $table->increments('id_jual_retur');
            $table->integer('id_jual');
            $table->integer('id_barang');
            $table->date('tanggal');
            $table->integer('kuantitas');
            $table->double('potongan', 11,2)->nullable()->default(0);
            $table->double('harga', 11,2);
            $table->double('total', 11,2);
            $table->string('alasan');
            $table->integer('status')->default(0);
            $table->string('author');
        });
    }

    public function down()
    {
        Schema::dropIfExists('jual_retur');
    }
}
