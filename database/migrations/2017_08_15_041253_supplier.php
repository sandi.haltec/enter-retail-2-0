<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Supplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function(Blueprint $table) {
            $table->increments('id_supplier');
            $table->string('nama_supplier', 64);
            $table->string('alamat');
            $table->string('no_telp');
            $table->string('email', 64);
            $table->integer('penjualan')->nullable();
            $table->datetime('waktu_transaksi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
