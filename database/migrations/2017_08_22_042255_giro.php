<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Giro extends Migration
{

    public function up()
    {
        Schema::create('giro', function(Blueprint $table) {
            $table->increments('id_giro');
            $table->integer('id_penjualan');
            $table->string('no_ref');
            $table->string('no_faktur');
            $table->date('tanggal_buat');
            $table->date('tanggal_cair')->nullable();
            $table->date('tanggal_hangus')->nullable();
            $table->date('jatuh_tempo');
            $table->double('total');
            $table->string('jenis')->default('pembelian');
            $table->integer('status')->default(0);
            $table->string('author');
        });
    }

    public function down()
    {
        Schema::dropIfExists('giro');
    }
}
