<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JualBayar extends Migration
{

    public function up()
    {
        Schema::create('jual_bayar', function(Blueprint $table){
            $table->increments('id_jual_bayar');
            $table->integer('id_jual');
            $table->integer('id_bank')->nullable()->default(0);
            $table->date('tanggal');
            $table->string('metode');
            $table->string('ref');
            $table->string('status');
            $table->double('jumlah', 11,2);
            $table->string('author');
        });
    }

    public function down()
    {
        Schema::dropIfExists('jual_bayar');
    }
}
