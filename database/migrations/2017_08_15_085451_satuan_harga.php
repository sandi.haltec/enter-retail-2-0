<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SatuanHarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuan_harga', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('id_satuan_jual');
            $table->integer('id_satuan_beli');
            $table->string('kode_barang');
            $table->double('harga_beli', 11,2);
            $table->double('harga_jual', 11,2);
            $table->double('pengali', 11,2)->nullable();
            $table->double('laba', 11,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuan_harga');
    }
}
