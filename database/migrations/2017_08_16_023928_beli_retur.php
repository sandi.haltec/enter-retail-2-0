<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeliRetur extends Migration
{

    public function up()
    {
        Schema::create('beli_retur', function(Blueprint $table) {
            $table->increments('id_beli_retur');
            $table->integer('id_beli');
            $table->integer('id_barang');
            $table->date('tanggal');
            $table->integer('kuantitas')->nullable();
            $table->double('diskon', 11,2)->nullable()->default(0);
            $table->double('harga', 11,2);
        });
    }

    public function down()
    {
        Schema::dropIfExists('beli_retur');
    }
}
