<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BarangStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_stok', function(Blueprint $table) {
            $table->increments('id_harga_barang');
            $table->string('kode_barang');
            $table->integer('id_satuan_jual');
            $table->double('stok_masuk', 11,2)->default(0);
            $table->double('stok_keluar', 11,2)->default(0);
            $table->double('stok_saat_ini', 11,2)->default(0);
            $table->double('stok_minimal_keluar', 11,2)->default(0);
            $table->double('stok_minimal', 11,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_stok');
    }
}
