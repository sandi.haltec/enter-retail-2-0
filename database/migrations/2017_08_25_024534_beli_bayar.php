<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeliBayar extends Migration
{

    public function up()
    {
        Schema::create('beli_bayar', function(Blueprint $table) {
            $table->increments('id_beli_bayar');
            $table->integer('id_beli');
            $table->date('tanggal');
            $table->string('metode');
            $table->string('ref');
            $table->double('jumlah', 11,2)->default(0);
            $table->string('status');
            $table->string('author')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('beli_bayar');
    }
}
