<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeliLain extends Migration
{

    public function up()
    {
        Schema::create('beli_lain', function(Blueprint $table) {
            $table->increments('id_beli_lain');
            $table->integer('id_beli');
            $table->double('harga')->nullable()->default(0);
            $table->string('deskripsi')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('beli_lain');
    }
}
