<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Beli extends Migration
{
    public function up()
    {
        Schema::create('beli', function(Blueprint $table) {
            $table->increments('id_beli');
            $table->string('no_faktur');
            $table->date('tanggal');
            $table->date('jatuh_tempo');
            $table->integer('id_supplier');
            $table->double('diskon', 11,2)->nullable()->default(0);
            $table->double('ppn', 11,2)->nullable()->default(0);
            $table->double('total', 11,2)->default(0);
            $table->string('status');
            $table->string('tangguhkan')->nullable();
            $table->string('author');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('beli');
    }
}
