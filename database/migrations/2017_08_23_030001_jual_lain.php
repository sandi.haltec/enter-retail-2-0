<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JualLain extends Migration
{

    public function up()
    {
        Schema::create('jual_lain', function(Blueprint $table){
            $table->increments('id_jual_lain');
            $table->integer('id_jual');
            $table->double('harga', 11,2);
            $table->string('deskripsi')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jual_lain');
    }
}
