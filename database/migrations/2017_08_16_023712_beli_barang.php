<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeliBarang extends Migration
{
    public function up()
    {
        Schema::create('beli_barang', function(Blueprint $table) {
            $table->increments('id_beli_barang');
            $table->integer('id_beli');
            $table->integer('id_barang');
            $table->integer('id_satuan_beli');
            $table->double('harga', 11,2)->default(0);
            $table->double('jumlah', 11,2)->default(0);
            $table->double('diskon', 11,2)->nullable()->default(0);
            $table->double('prosen_ppn', 11,2)->nullable()->default(0);
            $table->double('ppn', 11,2)->nullable()->default(0);
            $table->double('total', 11,2)->default(0);
            $table->date('expired');
        });
    }

    public function down()
    {
        Schema::dropIfExists('beli_barang');
    }
}
