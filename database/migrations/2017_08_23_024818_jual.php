<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jual extends Migration
{
    public function up()
    {
        Schema::create('jual', function(Blueprint $table) {
            $table->increments('id_jual');
            $table->string('no_faktur');
            $table->date('tanggal');
            $table->date('jatuh_tempo');
            $table->integer('id_pelanggan');
            $table->double('potongan', 11,2)->nullable()->default(0);
            $table->double('ppn', 11,2)->nullable()->default(0);
            $table->double('total', 11,2);
            $table->string('tipe');
            $table->string('status');
            $table->string('tangguhkan')->nullable();
            $table->string('author');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jual');
    }
}
