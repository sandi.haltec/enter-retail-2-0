<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BukuHarga extends Migration
{

    public function up()
    {
        Schema::create('buku_harga', function(Blueprint $table) {
            $table->increments('id_buku_harga');
            $table->integer('id_barang');
            $table->integer('id_pelanggan');
            $table->double('harga');
        });
    }

    public function down()
    {
        Schema::dropIfExists('buku_harga');
    }
}
