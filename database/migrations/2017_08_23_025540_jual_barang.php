<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JualBarang extends Migration
{

    public function up()
    {
        Schema::create('jual_barang', function(Blueprint $table) {
            $table->increments('id_jual_barang');
            $table->integer('id_jual');
            $table->integer('id_barang');
            $table->double('harga', 11,2)->nullable();
            $table->integer('kuantitas');
            $table->double('potongan', 11,2)->nullable()->default(0);
            $table->double('ppn', 11,2)->nullable()->default(0);
            $table->double('total', 11,2);
        });
    }

    public function down()
    {
        Schema::dropIfExists('jual_barang');
    }
}
