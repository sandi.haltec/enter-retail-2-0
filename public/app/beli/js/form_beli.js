$(function() {
    $('.date').datepicker({
        format : 'dd-mm-yyyy'
    });
    $($('input[name="nama_barang"]')).autocomplete({
        source: api_url + 'barang/search',
        select: function(event, ui) {
            selectBarang(ui.item.id);
        }
    });
    $($('input[name="nama_supplier"]')).autocomplete({
        source: api_url + 'supplier/search',
        select: function(event, ui) {
            selectSupplier(ui.item.id);
        }
    });
});
function searchSupplier() {
    $('#modalSearchSupplier').modal('show');
    $('#tableSearchSupplier').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url+'api/supplier/get',
        columns:[
            {data: 'nama_supplier', name: 'nama_supplier'},
            {data: 'alamat', name: 'alamat'},
            {data: 'no_telp', name: 'no_telp'},
            {data: 'email', name: 'email'},
            {data: 'id_supplier', name: 'id_supplier', orderable: false, render: function(data, type, row) {
                return html = '<a onclick="selectSupplier('+data+')" class="btn btn-sm btn-default"><i class="fa fa-check"></i></a>';
            }},
        ]
    });      
}

function selectSupplier(id) {
    $.ajax({
        url:base_url+'api/supplier/find',
        type:'post',
        dataType:'json',
        data:'id='+id,
        success:function(response){
            $("input[name='id_supplier']").val(response['id_supplier']);
            $("input[name='nama_supplier']").val(response['nama_supplier']);
            $("#alamat_supplier").val(response['alamat']);
            $("input[name='no_telp']").val(response['no_telp']);
            $("input[name='email']").val(response['email']);
            $('#modalSearchSupplier').modal('hide');
        }
    });
}

function checkSupplier() {
    
}
function searchBarang() {
    $('#modalSearchBarang').modal('show');
    $('#tableSearchBarang').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url+'api/barang/get',
        columns:[
            {data: 'barcode', name: 'barcode'},
            {data: 'nama_barang', name: 'nama_barang'},
            {data: 'kode_barang', name: 'kode_barang'},
            {data: 'id_barang', name: 'id_barang', orderable: false, render: function(data, type, row) {
                return html = '<a onclick="selectBarang('+data+')" class="btn btn-sm btn-default"><i class="fa fa-check"></i></a>';
            }},
        ]
    });
}

function selectBarang(id) {
    $.ajax({
        url:base_url+'api/barang/find',
        type:'post',
        dataType:'json',
        data:'id='+id,
        success:function(response){
            $("input[name='nama_barang']").val(response['nama_barang']);
            $("input[name='jumlah']").val(1);
            $("input[name='id_barang']").val(response['id_barang']);
            $("input[name='satuan_beli']").val(response['satuan_beli']);
            $("input[name='id_satuan_beli']").val(response['id_satuan_beli']);
            $("input[name='harga_beli']").val(response['harga_beli']);
            $("input[name='prosen_ppn']").val(response['ppn']);
            $('#modalSearchBarang').modal('hide');
            countTotalItem();
        }
    });
}

function checkInputValue() {
    var jumlah = $('input[name="jumlah"]').val();
    if(!$.isNumeric(jumlah)) {
        swalAlert('Inputan harus berupa angka!');
    }
    var prosen_ppn = $('input[name="prosen_ppn"]').val();
    if(!$.isNumeric(prosen_ppn)) {
        swalAlert('Inputan harus berupa angka!');
    }
}

function countTotalItem() {
    var jumlah = $('input[name="jumlah"]').val();
    var harga_beli = $('input[name="harga_beli"]').val();
    var diskon = $('input[name="diskon"]').val();
    var total = (jumlah * harga_beli) - diskon;
    var prosen_ppn = $('input[name="prosen_ppn"]').val();
    var ppn = total * (prosen_ppn / 100);
    $('input[name="total"]').val(total);
    $('input[name="ppn"]').val(ppn);
}

function addList() {
    var html = '';
    var id_barang = $('input[name="id_barang"]').val();
    var nama_barang = $('input[name="nama_barang"]').val();
    var expired = $('input[name="expired"]').val();
    var jumlah = $('input[name="jumlah"]').val();
    var satuan_beli = $('input[name="satuan_beli"]').val();
    var id_satuan_beli = $('input[name="id_satuan_beli"]').val();
    var harga_beli = $('input[name="harga_beli"]').val();
    var diskon = $('input[name="diskon"]').val();
    var total = $('input[name="total"]').val();
    var prosen_ppn = $('input[name="prosen_ppn"]').val();
    var ppn = $('input[name="ppn"]').val();
    var grand_ppn = $('input[name="grand_ppn"]').val();
    var totalList = $('#table-list-beli tbody tr.list-rows').length;
    if(!$.isNumeric(jumlah)) {
        return swalAlert('Inputan harus berupa angka!');
    }
    $.ajax({
        url:base_url+'api/barang/find',
        type:'post',
        dataType:'json',
        data:'id='+id_barang,
        success:function(response){
            if(id_barang == response['id_barang']) {
            html += '<tr class="list-rows" id="list-row-'+totalList+'">' +
                        '<td>' +
                            nama_barang +
                            '<input type="hidden" name="barang['+totalList+'][id_barang]" id="list-row-id_barang-'+totalList+'" value="'+id_barang+'"/>' +
                            '<input type="hidden" name="barang['+totalList+'][nama_barang]" id="list-row-nama_barang-'+totalList+'" value="'+nama_barang+'"/>' +
                        '</td>' +
                        '<td>' +
                            expired +
                            '<input type="hidden" name="barang['+totalList+'][expired]" id="list-row-expired-'+totalList+'" value="'+expired+'"/>' +
                        '</td>' +
                        '<td class="text-center">' +
                            jumlah +
                            '<input type="hidden" name="barang['+totalList+'][jumlah]" id="list-row-jumlah-'+totalList+'" value="'+jumlah+'"/>' +
                        '</td>' +
                        '<td>' +
                            satuan_beli +
                            '<input type="hidden" name="barang['+totalList+'][id_satuan_beli]" id="list-row-satuan_beli-'+totalList+'" value="'+id_satuan_beli+'"/>'+
                        '</td>' +
                        '<td class="text-right">' + 
                            harga_beli +
                            '<input type="hidden" name="barang['+totalList+'][harga_beli]" id="list-row-harga_beli-'+totalList+'" value="'+harga_beli+'"/>'+
                        '</td>' +
                        '<td class="text-right">' +
                            diskon +
                            '<input type="hidden" name="barang['+totalList+'][diskon]" id="list-row-diskon-'+totalList+'" value="'+diskon+'"/>'+
                        '</td">' +
                        '<td class="text-right">'+
                            total +
                            '<input type="hidden" name="barang['+totalList+'][total]" id="list-row-total-'+totalList+'" value="'+total+'"/>'+
                        '</td>' +
                        '<td>' +
                            prosen_ppn +
                            '<input type="hidden" name="barang['+totalList+'][prosen_ppn]" id="list-row-prosen_ppn-'+totalList+'" value="'+prosen_ppn+'"/>'+
                        '</td>' +
                        '<td>' +
                            ppn +
                            '<input type="hidden" name="barang['+totalList+'][ppn]" id="list-row-ppn-'+totalList+'" value="'+ppn+'"/>'+
                        '</td>' +
                        '<td><button type="button" onclick="removeList('+totalList+')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>' +
                    '</tr>';
            $('#table-list-beli tbody #list-beli-item').after(html);
            nullAbleInputList();
            countTotalItem();
            countGrandTotalItem();
            } else {
                swalAlert('Barang tidak ditemukan.');
            }
        }
    });
}

function countGrandTotalItem() {
    var length = $('#table-list-beli tbody tr.list-rows').length;
    var grand_total_item = 0;
    var total_grand_ppn = 0;
    for (var i = length - 1; i >= 0; i--) {
        var diskon = $('input[name="barang['+i+'][diskon]"]').val();
        var ppn = $('input[name="barang['+i+'][ppn]"]').val();
        var total = $('input[name="barang['+i+'][total]"]').val();
        if(!$.isNumeric(diskon)) {
            diskon = 0;
        } 
        if(!$.isNumeric(ppn)) {
            ppn = 0;
        }
        diskon = parseFloat(diskon);
        ppn = parseFloat(ppn);
        total = parseFloat(total);
        grand_total_item += parseFloat(total);
        total_grand_ppn += parseFloat(ppn);
    }
    $('input[name="grand_total"]').val(grand_total_item);
    $('input[name="grand_ppn"]').val(total_grand_ppn);
    $('input[name="float_grand_ppn"]').val(total_grand_ppn);
    countGrandTotal();
}

function removeList(idx) {
    $('#list-row-'+idx).remove();
    countGrandTotalItem();
}

function nullAbleInputList() {
    $('input[name="id_barang"]').val('');
    $('input[name="nama_barang"]').val('');
    $('input[name="jumlah"]').val('');
    $('input[name="satuan_beli"]').val('');
    $('input[name="harga_beli"]').val('');
    $('input[name="diskon"]').val(0);
    $('input[name="total"]').val('');
    $('input[name="int_ppn"]').val(0);
}

function checkBarang() {

}

function showModalPembayaran() {
    $('#modalPembayaran').modal('show');
}

var grandTotal = 0;
function addListPembayaranLain() {
    var keterangan = $('input[name="keterangan"]').val();
    var harga = $('input[name="harga"]').val();
    if(keterangan == '' && harga == '') {
        swalAlert('Keterangan & Harga tidak boleh kosong!');
    } else {
        var lengthRow = $('#table-list-pembayaran-lain tbody .tr-list-pembayaran-lain-row').length;
        var html = '';
        html += '<tr class="tr-list-pembayaran-lain-row" id="tr-list-pembayaran-lain-row-'+lengthRow+'">' +
                    '<td>' +
                        keterangan +
                        '<input type="hidden" name="pembayaran_lain['+lengthRow+'][keterangan]" value="'+keterangan+'"/>' +
                    '</td>' +
                    '<td>' +
                        harga +
                        '<input type="hidden" name="pembayaran_lain['+lengthRow+'][harga]" value="'+harga+'"/>' +
                    '</td>' +
                    '<td>' +
                       '<button type="button" onclick="deleteListPembayaranLain('+lengthRow+')" class="btn btn-danger"><i class="fa fa-trash"></i></button>'
                    '</td>' +
                '</tr>';
        $('#table-list-pembayaran-lain tbody #tr-list-pembayaran-lain').after(html);
        $('input[name="keterangan"]').val('');
        $('input[name="harga"]').val('');
        countGrandTotal();
        countListPembayaranLain();
    }
}

function countListPembayaranLain() {
    var length = $('#table-list-pembayaran-lain tbody .tr-list-pembayaran-lain-row').length;
    var totalPembayaranLain = 0;
    for (var i = length - 1; i >= 0; i--) {
        var harga = $('input[name="pembayaran_lain['+i+'][harga]"]').val();
        harga = parseFloat(harga);
        totalPembayaranLain += harga;
    }
    $('input[name="biaya_lain"]').val(totalPembayaranLain);
}

function deleteListPembayaranLain(idx) {
    $('#tr-list-pembayaran-lain-row-'+idx).remove();
    countListPembayaranLain();
    countGrandTotal();
}

var totalPembayaran = 0;
function addListPembayaran() {
    var grand_total_setelah_diskon = $('input[name="grand_total_setelah_diskon"]').val();
    var tanggal_pembayaran = $('input[name="tanggal_pembayaran"]').val();
    var metode = $('select[name="metode"] option:selected').text();
    var id_metode = $('select[name="metode"] option:selected').val();
    var referensi = $('input[name="referensi"]').val();
    var jumlah = $('input[name="jumlah_pembayaran"]').val();
    var lengthRow = $('#table-list-pembayaran tbody .tr-list-pembayaran-row').length;
    if(tanggal_pembayaran != null && jumlah != null) {
        if(jumlah <= grand_total_setelah_diskon) {
            var html = '<tr id="tr-list-pembayaran-row-'+lengthRow+'" class="tr-list-pembayaran-row">' +
                        '<td>' +
                            tanggal_pembayaran +
                            '<input type="hidden" name="pembayaran['+lengthRow+'][tanggal_pembayaran]" value="'+tanggal_pembayaran+'">' +
                        '</td>' +
                        '<td>' +
                            metode +
                            '<input type="hidden" name="pembayaran['+lengthRow+'][metode]" value="'+id_metode+'">' +
                        '</td>' +
                        '<td>' +
                            referensi +
                            '<input type="hidden" name="pembayaran['+lengthRow+'][referensi]" value="'+referensi+'">' +
                        '</td>' +
                        '<td>' +
                            jumlah +
                            '<input type="hidden" name="pembayaran['+lengthRow+'][jumlah]" value="'+jumlah+'">' +
                        '</td>' +
                        '<td>' +
                            '<button type="button" onclick="deleteListPembayaran('+lengthRow+')" class="btn btn-md btn-danger"><i class="fa fa-trash"></i></button>' +
                        '</td>' +
                    '</tr>';
            $('#table-list-pembayaran tbody #tr-list-pembayaran').after(html);
            $('input[name="referensi"]').val('');
            $('input[name="jumlah_pembayaran"]').val('');
            countListPembayaran();
            payment();
        } else {
            swalAlert('Jumlah pembayaran melebihi total!');
        }
    } else {

    }
}

function countListPembayaran() {
    var length = $('#table-list-pembayaran tbody .tr-list-pembayaran-row').length;
    var totalPembayaran = 0;
    for (var i = length - 1; i >= 0; i--) {
        var harga = $('input[name="pembayaran['+i+'][jumlah]"]').val();
        harga = parseFloat(harga);
        totalPembayaran += harga;
    }
    $('input[name="total_pembayaran"]').val(totalPembayaran);
    $('input[name="float_total_pembayaran"]').val(totalPembayaran);
}

function deleteListPembayaran(idx) {
    var jumlah_lama = $('input[name="pembayaran['+idx+'][jumlah]"]').val();
    var total_pembayaran = $('input[name="total_pembayaran"]').val();
    total_pembayaran -= jumlah_lama;
    $('input[name="total_pembayaran"]').val(total_pembayaran);
    $('input[name="float_total_pembayaran"]').val(total_pembayaran);
    payment();
    $('#tr-list-pembayaran-row-'+idx).remove();
    countListPembayaran();
}

function countGrandTotal() {
    var grand_total = $('input[name="grand_total"]').val();
    var grand_diskon = $('input[name="grand_diskon"]').val();
    var grand_ppn = $('input[name="grand_ppn"]').val();
    var biaya_lain = $('input[name="biaya_lain"]').val();
    if(!$.isNumeric(grand_total)){
        grand_total=0;
    }
    if(!$.isNumeric(grand_diskon)){
        grand_diskon=0;
    }
    if(!$.isNumeric(grand_ppn)){
        grand_ppn=0;
    }
    if(!$.isNumeric(biaya_lain)){
        biaya_lain=0;
    }
    grand_total=parseFloat(grand_total);
    grand_diskon=parseFloat(grand_diskon);
    grand_ppn=parseFloat(grand_ppn);
    biaya_lain=parseFloat(biaya_lain);
    if(grand_diskon <= grand_total) {
        $('input[name="float_grand_diskon"]').val(grand_diskon);
        $('input[name="grand_total_setelah_diskon"]').val(grand_total-grand_diskon+grand_ppn+biaya_lain);
        $('input[name="float_grand_total_setelah_diskon"]').val(grand_total-grand_diskon+grand_ppn+biaya_lain);
    } else {
        swalAlert('Diskon tidak boleh melebihi total!');
    }
    payment();
}

function payment() {
    var grand_total_setelah_diskon = $('input[name="grand_total_setelah_diskon"]').val();
    var total_pembayaran = $('input[name="total_pembayaran"]').val();
    if(!$.isNumeric(grand_total_setelah_diskon)) {
        grand_total_setelah_diskon = 0;
    }
    if(!$.isNumeric(total_pembayaran)) {
        total_pembayaran = 0;
    }
    grand_total_setelah_diskon = parseFloat(grand_total_setelah_diskon);
    total_pembayaran = parseFloat(total_pembayaran);
    $('input[name="selisih_pembayaran"]').val(total_pembayaran - grand_total_setelah_diskon);
    $('input[name="float_selisih_pembayaran"]').val(total_pembayaran - grand_total_setelah_diskon);
}

function submit() {
    $.ajax({
        url:base_url+'api/pembelian/store',
        type:'post',
        dataType:'json',
        data:$('form').serialize(),
        success:function(response){
            if(response['success']) {
                window.location.href = base_url + 'pembelian';
            } else {
                $('.message').html('<div class="alert alert-danger">' +
                    '<strong>Terjadi Kesalahan Input! </strong>' +
                    '<ul>'+
                        $.each(response['validation'], function(val) {
                            '<li>' +
                                val
                            '</li>'
                        }) +
                    '</ul>' +
                '</div>');
            }
            $('#modalPembayaran').modal('hide');
        }
    });
}
$('.input-number').number(true, 2);