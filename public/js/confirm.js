var base_url = 'http://localhost:8000/';
function confirm(url) {
    swal({
        title: "Apakah Anda yakin?",
        text: "Data yang telah dihapus tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    },
    function(isConfirm){
        if(isConfirm) {
            window.location.href = base_url + url;
        }
    });
}

function swalAlert(text) {
    sweetAlert("Whoops!", text);
}